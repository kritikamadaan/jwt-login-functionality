import React from 'react';
// javascript plugin used to create scrollbars on windows
// import PerfectScrollbar from 'perfect-scrollbar';
import {
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
import { connect } from "react-redux"
import { Header, Footer, Sidebar, ChatSidebar, Stylebar } from 'components'

import dashboardRoutes from 'routes/hospital.jsx';
import { topbarStyle, menuStyle, menuType, topbarType, navWidth, chatWidth, chatType } from 'variables/settings/hospital.jsx';

import EditPatient from 'views/hospital/Patient/EditPatient.jsx';
import EditDoctor from 'views/hospital/Doctor/EditDoctor.jsx';
import EditClinic from '../views/hospital/Clinic/EditCinic';


import {Row,Col} from "reactstrap"
import EditingProcedure from '../views/Procedures/EditingProcedures';
import Editappointment from '../views/Appointments/Editappointment';

var BASEDIR = process.env.REACT_APP_BASEDIR;

class EditProfileLayout extends React.Component {
    constructor(props) {
        super(props);
        this.menuSettings = this.menuSettings.bind(this);
        this.topbarSettings = this.topbarSettings.bind(this);
    }

    state = {
        token: window.localStorage.getItem("token"),
        menuColor: menuStyle,
        topbarColor: topbarStyle,
        menuType: menuType,
        topbarType: topbarType,
    }

    componentDidMount() {
        const token = this.state.token
        // console.log(this.props.match.params.eId, ">>>>>>>>>", token)
        fetch(`/doctors/details/${this.props.match.params.eId}/`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            },
        })
            .then(res => res.json())
            .then(res => {
                // console.log(res)
                this.props.dispatch({
                    type: "EDIT_DOCTOR_PROFILE",
                    editDoctor: res,
                    edit:true
                });
            })
    }

    menuSettings(val1, val2) {
        this.setState({
            menuColor: val1,
            menuType: val2,
        });
    }
    topbarSettings(val1, val2) {
        this.setState({
            topbarColor: val1,
            topbarType: val2,
        });
    }

    componentDidUpdate(e) {
        if (e.history.action === "PUSH") {
            this.refs.mainPanel.scrollTop = 0;
            document.scrollingElement.scrollTop = 0;
        }
    }
    render() {
        // console.log(">>>>>>>>>>>>>>>>>>>>.", this.props.match.params.id)
        const eMatch = this.props.match.params.ematch

        return (
            <div className="wrapper" ref="themeWrapper" data-menu={this.state.menuColor} data-topbar={this.state.topbarColor} data-menutype={this.state.menuType} data-topbartype={this.state.topbarType}>
        
                <Header {...this.props} navtype={navWidth} admintype={'hospital'} />
                <Sidebar {...this.props} routes={dashboardRoutes} admintype={'hospital'} />
                <div className="main-panel" ref="mainPanel">
                    {
                        eMatch === "doctor" ? <EditDoctor id={this.props.match.params.eId} /> :
                            eMatch === "patient" ? <EditPatient id={this.props.match.params.eId} /> : eMatch === "clinic"?
                            <EditClinic id={this.props.match.params.eId}/>: eMatch === "procedure" ?
                            <EditingProcedure id={this.props.match.params.eId}/> :
                            eMatch === "appointment" ? <Editappointment id={this.props.match.params.eId}/>
                            :null
                    }
                    <Footer fluid />
                </div>
                <ChatSidebar {...this.props} routes={dashboardRoutes} chatwidth={chatWidth} chattype={chatType} />
                <Stylebar menuSettings={this.menuSettings} topbarSettings={this.topbarSettings} />
            </div>
        );
    }
}

export default connect()(EditProfileLayout);
