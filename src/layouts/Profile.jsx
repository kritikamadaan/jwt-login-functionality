import React from 'react';
// javascript plugin used to create scrollbars on windows
// import PerfectScrollbar from 'perfect-scrollbar';
import {
    Route,
    Switch,
    Redirect
} from 'react-router-dom';

import { Header, Footer, Sidebar, ChatSidebar, Stylebar } from 'components'

import dashboardRoutes from 'routes/hospital.jsx';
import { topbarStyle, menuStyle, menuType, topbarType, navWidth, chatWidth, chatType } from 'variables/settings/hospital.jsx';

import DoctorProfile from 'views/hospital/Doctor/DoctorProfile.jsx';

import PatientProfile from 'views/hospital/Patient/PatientProfile.jsx';
import DetailsClinic from '../views/hospital/Clinic/DetailsClinic';

import AppointmentProfile from '../views/Appointments/AppointmentProfile';
import ProcedureProfile from '../views/Procedures/ProcedureProfile';
import SpecialityProfile from '../views/hospital/Speciality/SpecialityProfile';



var BASEDIR = process.env.REACT_APP_BASEDIR;

class ProfileLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menuColor: menuStyle,
            topbarColor: topbarStyle,
            menuType: menuType,
            topbarType: topbarType,
        };
        this.menuSettings = this.menuSettings.bind(this);
        this.topbarSettings = this.topbarSettings.bind(this);
    }

    menuSettings(val1, val2) {
        this.setState({
            menuColor: val1,
            menuType: val2,
        });
    }
    topbarSettings(val1, val2) {
        this.setState({
            topbarColor: val1,
            topbarType: val2,
        });
    }
    // componentDidMount(){
    //     console.log("..........cdid")
    // }
    componentDidUpdate(e) {
        if (e.history.action === "PUSH") {
            this.refs.mainPanel.scrollTop = 0;
            document.scrollingElement.scrollTop = 0;
        }
    }
    render() {
        // console.log(">>>>>>>>>>>>>>>>>>>>.", this.props.match.params.id)
        const match = this.props.match.params.match

        return (
    
            <div className="wrapper" ref="themeWrapper" data-menu={this.state.menuColor} data-topbar={this.state.topbarColor} data-menutype={this.state.menuType} data-topbartype={this.state.topbarType}>
            
                <Header {...this.props} navtype={navWidth} admintype={'hospital'} />
                <Sidebar {...this.props} routes={dashboardRoutes} admintype={'hospital'} />
                <div className="main-panel" ref="mainPanel">
                    {
                        match === "doctor" ? <DoctorProfile id={this.props.match.params.id} /> :
                            match === "patient" ? <PatientProfile id={this.props.match.params.id} /> :
                                match === "clinic" ? <DetailsClinic id={this.props.match.params.id} /> :
                                match === "appointment" ? <AppointmentProfile id={this.props.match.params.id}/> :
                                match === "procedure" ? <ProcedureProfile id={this.props.match.params.id}/> :
                                match ===  "speciality" ? <SpecialityProfile id={this.props.match.params.id}/> :
                                match === "procedure" ? null :null
                    }
                    <Footer fluid />
                </div>
                <ChatSidebar {...this.props} routes={dashboardRoutes} chatwidth={chatWidth} chattype={chatType} />
                <Stylebar menuSettings={this.menuSettings} topbarSettings={this.topbarSettings} />
            </div>
        );
    }
}

export default ProfileLayout;
