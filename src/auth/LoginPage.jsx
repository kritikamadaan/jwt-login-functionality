import React from 'react';
// javascript plugin used to create scrollbars on windows

import Login from "./pages/login.jsx"

class LoginPage extends React.Component {
   
    render() {
        return (
            <div className="wrapper login_page">

                <div className="main-panel" ref="mainPanel">
                    <Login />  
                </div>
            </div>
        );
    }
}

export default LoginPage;
