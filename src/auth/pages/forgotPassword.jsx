import React from 'react';
import {
    Row, Col,
} from 'reactstrap';
import { Link } from 'react-router-dom';

var BASEDIR = process.env.REACT_APP_BASEDIR;

class ForgotPassword extends React.Component {


    render() {

        return (
            <div className="wrapper login_page">
                <div className="main-panel" ref="mainPanel">
                    <div>
                        <div className="">
                            <Row>
                                <Col xs={12} md={12}>
                                    <div className="container-fluid">
                                        <div className="register-wrapper row">
                                            <div id="register" className="login loginpage offset-xl-4 offset-lg-3 offset-md-3 offset-0 col-12 col-md-6 col-xl-4">
                                                <h1><a href="#!" title="Login Page" tabIndex="-1">&nbsp;</a></h1>

                                                <form name="loginform" id="loginform" action="#!" method="post">
                                                    <p>
                                                        <label htmlFor="user_login3">Old Password<br />
                                                            <input type="text" name="log" id="user_login3" className="input" size="20" /></label>
                                                    </p>
                                                    <p>
                                                        <label htmlFor="user_pass">New Password<br />
                                                            <input type="password" name="pwd" id="user_pass" className="input" size="20" /></label>
                                                    </p>
                                                    <p>
                                                        <label htmlFor="user_pass2">Confirm Password<br />
                                                            <input type="password" name="pwd1" id="user_pass2" className="input" size="20" /></label>
                                                    </p>
                                                    <p className="submit">
                                                        <input type="button" name="wp-submit" id="wp-submit" className="btn btn-accent btn-block" value="Sign Up" />
                                                    </p>
                                                </form>

                                                <p id="nav">
                                                    <Link to={BASEDIR + "/login"} title="Sign In">Sign In</Link>
                                                </p>
                                                <div className="clearfix"></div>

                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ForgotPassword;
