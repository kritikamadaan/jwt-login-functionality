import React from 'react';
// javascript plugin used to create scrollbars on windows
import {
    Row, Col,
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import { connect } from "react-redux";

var BASEDIR = process.env.REACT_APP_BASEDIR;

class Login extends React.Component {
    state = {
        userData: {
            email: "",
            password: ""
        },
        signInError: {
            email: "",
            password: "",
            non_field_errors: ""
        },
        pageRedirectStatus: false
    };
    handleInputChange = (e, name) => {
        const value = e.target.value;
        this.setState({
            userData: { ...this.state.userData, [name]: value }
        });
    };
    handleLoginSubmit = () => {
        const userData = this.state.userData;
        let errorMassege = {}
        // csrfToken = getCookie("csrftoken");
        // console.log(csrfToken)
        if (this.state.userData.email && this.state.userData.password) {
            fetch(`/users/login/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(userData)
            })
                .then(res => res.json())
                .then(response => {
                    const token = response.token
                    
                    if (token) {
                        // csrfToken = getCookie("csrftoken");
                        fetch(`/users/list/`, {
                            method: "GET",
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": "Bearer " + token
                            },
                        })
                            .then(res => res.json())
                            .then(res => {
                                window.localStorage.setItem("token", token);
                                // localStorage.removeItem('token');
                                fetch(`/comman/data/`, {
                                    method: "GET",
                                    headers: {
                                        "Content-Type": "application/json",
                                        "Authorization": "Bearer " + token
                                    },
                                })
                                    .then(res => res.json())
                                    .then(response => {
                                        this.props.dispatch({
                                            type: "ALL_DOCTOR_LIST",
                                            doctor: response.doctor
                                        })

                                        this.props.dispatch({
                                            type: "All_SPECIALITY",
                                            Speciality: response.speciality
                                        });

                                        this.props.dispatch({
                                            type: "ADD_ALL_CLINIC",
                                            clinic: response.clinic
                                        })

                                        this.props.dispatch({
                                            type: "ALL_PATIENT_DATA",
                                            patient: response.patient
                                        })
                                        this.props.dispatch({
                                            type: "ALL_APOINTMENTS_LIST",
                                            appointment: response.appointments
                                        })
                                        this.props.dispatch({
                                            type: "ADD_ALL_PROCEDURES",
                                            procedure: response.procedures
                                        })
                                        
                                    })

                                // set user details
                                let userDetails = res.email
                               
                                this.props.dispatch({
                                    type: "SUPER_USER_INFO",
                                    userDetails,
                                    token
                                });
                                this.props.dispatch({
                                    type: "TOTAL_HOSPITAL_DATA",
                                    total: res.total
                                });
                            })
                    } else {
                       
                        // this.setState({
                        // 	signInError: { ...this.state.signInError, ["non_field_errors"]:response.non_field_errors[0]  }
                        // })
                    }
                });
        } else {
            alert("emailand pass requieded")
        }
    }

    render() {
        if (this.props.email) {
          
            return <Redirect to={BASEDIR + "/"} />;
        }

        return (
            <div>
                <div className="">
                    <Row>
                        <Col xs={12} md={12}>
                            <div className="container-fluid">
                                <div className="login-wrapper row">
                                    <div id="login" className="login loginpage offset-xl-4 offset-lg-3 offset-md-3 offset-0 col-12 col-md-6 col-xl-4">
                                        <h1><a href="#!" title="Login Page" tabIndex="-1">&nbsp;</a></h1>

                                        <form>
                                            <p>
                                                <label htmlFor="user_email">Email<br />
                                                    <input
                                                        type="email"
                                                        // name="un"
                                                        id="user_email"
                                                        className="form-control"
                                                        value={this.state.userData.email}
                                                        onChange={(e) => this.handleInputChange(e, "email")}
                                                    />
                                                </label>
                                            </p>
                                            <p>
                                                <label htmlFor="user_pass">Password<br />
                                                    <input
                                                        type="password"
                                                        // name="pwd"
                                                        id="user_pass"
                                                        className="input"
                                                        value={this.state.userData.password}
                                                        onChange={(e) => this.handleInputChange(e, "password")}
                                                    />
                                                </label>
                                            </p>
                                            <p>
                                                <input
                                                    type="button"
                                                    name="wp-submit"
                                                    id="wp-submit"
                                                    className="btn btn-accent btn-block"
                                                    value="Sign In"
                                                    onClick={() => this.handleLoginSubmit()}
                                                />
                                            </p>
                                        </form>

                                        <p id="nav">
                                            <Link to={BASEDIR + "/forgot/password"} title="Password Lost and Found">Forgot password?</Link> | <Link to={BASEDIR + "/register"} title="Sign Up">Sign Up</Link>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        email: state.auth.email
    };
};
export default connect(mapStateToProps)(Login);
