import React from 'react';
import {
    Row, Col,
} from 'reactstrap';
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom';


var BASEDIR = process.env.REACT_APP_BASEDIR;
var IMGDIR = process.env.REACT_APP_IMGDIR;

class AppointmentProfile extends React.Component {
    render() {
     const allAppointment = this.props.appointment;
        return (
            <div>
            <div className="content">
                <Row>
                    <Col xs={12} md={12}>

                        <div className="page-title">
                            <div className="float-left">
                                <h1 className="title">Appointment Profile</h1>
                            </div>
                        </div>
                        {
                            allAppointment ? allAppointment .map((item, i) => (
                                item.id === parseInt(this.props.id) ?
                                    <div key={item.id} className="col-xl-12">
                                        <section className="box profile-page">
                                            <div className="content-body">
                                                <div className="col-12">
                                                    <div className="row uprofile">
                                                        <div className="uprofile-image col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                                                            <img alt="" src={IMGDIR + "/images/hospital/doctors/doctor.jpeg"} className="img-fluid" />
                                                        </div>
                                                        <div className="uprofile-name col-xl-10 col-lg-9 col-md-9 col-sm-8 col-12">
                                                            <h3 className="uprofile-owner">
                                                                <p>Dr. {
                                                                    item.doctor.middle_name ?
                                                                        item.doctor.first_name.toUpperCase() + " " + item.doctor.middle_name.toUpperCase() + " " + item.doctor.last_name.toUpperCase() :
                                                                        item.doctor.first_name.toUpperCase() + " " + item.doctor.last_name.toUpperCase()
                                                                }
                                                                </p>
                                                            </h3>
                                                            <NavLink
                                                                className="btn btn-primary btn-sm profile-btn"
                                                                to={BASEDIR + `/dashboard/edit/${"appointment"}/${item.id}`}
                                                                title="edit profile"

                                                            >Edit
                                                            </NavLink>
                                                            <div className="clearfix"></div>
                                                            <p className="uprofile-title">{item.doctor.speciality.speciality_name}</p>
                                                            <div className="clearfix"></div>
                                                            <p>{item.notes}</p>
                                                            <p className="uprofile-list">
                                                                <span><i className='i-phone'></i> {item.doctor.mobile_number}</span>
                                                                <span><i className='i-envelope icon-primary icon-xs'></i> {item.doctor.email}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-12">
                                                    <div className="clearfix"></div>
                                                    <hr />
                                                    <h4>Personal Info:</h4>
                                                    <ul>
                                                        <ul>
                                                            <li>Age: {item.doctor.age} years old</li>
                                                            <li>Gender: {item.doctor.gender}</li>
                                                            <li>DOB: {item.doctor.dob}</li>
                                                            <li>E-mail: {" "}{item.doctor.email}</li>
                                                            <li>Mobile Number :{" "}{item.doctor.mobile_number}</li>
                                                            <li>Notes :{" "}{item.doctor.notes}</li>
                                                        </ul>
                                                    </ul>
                                                    <div className="clearfix"></div>
                                                    <hr />
                                                    <h4>Speciality:</h4>
                                                    <ul>
                                                        <h4>{item.doctor.speciality.speciality_name}</h4>
                                                        <ul>
                                                            {
                                                                item.procedures.map((p, j) => (
                                                                    <li>{p}</li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </ul>

                                                    <div className="clearfix"></div>
                                                    <hr />
                                                    <h4>Registration</h4>
                                                    <ul>
                                                        <ul>
                                                            <li>Number: {item.doctor.registration_number}</li>
                                                            <li>Years: {item.doctor.year_of_registration}</li>
                                                        </ul>
                                                    </ul>
                                                    <div className="clearfix"></div>
                            
                                                    <hr/>
                                                    <h4>Procedure:</h4>
                                                    <ul>
                                                        <ul>
                                                            {item.procedures.map((item,i)=>(
                                                                <li>{item}</li>
                                                            ))}
                                                        </ul>
                                                    </ul>
                                                    <div className="clearfix"></div>
                                                    <hr/>
                                                    <h4>Clinic Details:</h4>
                                                    <ul>
                                                        <ul>
                                                            <li>Clinic Name:{" "}{item.location.clinic_name}</li>
                                                            <li>Clinic Location:{" "}{item.location.clinic_location}</li>
                                                            <li>Clinic Latitude:{" "}{item.location.latitude}</li>
                                                            <li>Clinic Longitude:{" "}{item.location.longitude}</li>
                                                        </ul>
                                                    </ul>
                                                    <div className="clearfix"></div>
                                                    <hr/>
                                                    <h4>Patient Details:</h4>
                                                    <ul>
                                                        <ul>
                                                            <li>
                                                            Patient Name : {
                                                                    item.patient.middle_name ?
                                                                        item.patient.first_name.toUpperCase() + " " + item.patient.middle_name.toUpperCase() + " " + item.patient.last_name.toUpperCase() :
                                                                        item.patient.first_name.toUpperCase() + " " + item.patient.last_name.toUpperCase()
                                                                }
                                                            </li>
                                                            <li>Patient Age:{item.patient.age}</li>
                                                            <li>Patient Gender:{item.patient.age}</li>
                                                            <li>Date Of Birth: {item.patient.dob}</li>
                                                            <li>Email :{item.patient.email}</li>
                                                            <li>Mobile Number :{item.patient.mobile_number}</li>
                                                            <li>Adhar No: {item.patient.adhar_no}</li>
                                                            <li>PAN No : {item.patient.pan_no}</li>
                                                            <li>Occupation: {item.patient.occupation}</li>
                                                            <li>Birth City: {item.patient.place_of_birth_city}</li>
                                                            <li>Birth Country: {item.patient.place_of_birth_country}</li>
                                                            <li>Birth State :{item.patient.place_of_birth_state}</li>
                                                            <li>Referred By: {item.patient.referred_by}</li>
                                                            <li>Referred Date :{item.patient.referred_date}</li>
                                                        </ul>
                                                    </ul>
                                                    <div className="clearfix"></div>
                                                </div>
                                            </div>
                                        </section>
                                    </div> :
                                   null
                            )) : null
                        }
                    </Col>
                </Row>
            </div>
        </div>
        );
    }
}


const mapStateToProps = state => {
    return {
       appointment:state.appointment.allAppointments
    };
};
export default connect(mapStateToProps)(AppointmentProfile)

