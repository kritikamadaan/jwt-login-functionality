import React, { Component } from "react";
import { connect } from "react-redux";
import Select, { NonceProvider } from "react-select";
import { Row, Col, label, inputMask, input } from "reactstrap";

export class UpdateAppointment extends Component {
	state = {
		appointment: {
			location: "",
			patient: "",
			notes: "",
			status: "",
			start_date: "",
			end_date: "",
		},

		procedures: [],
		value: [],
		options: [],
		location: "",
		doctor: "",
		all_day: false,
		procedureDisable: false,
		token: window.localStorage.getItem("token"),
		fieldsError: "",
	};

	componentDidMount() {

		const token = this.state.token
		fetch(`/appointments/details/${this.props.id}/`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token
			},
		})
			.then(res => res.json())
			.then(response => {

				if (response.status) {
					this.setState({
						apppointment: response.data.id,
						doctor: response.data.doctor,
						patient: response.data.patient,

						procedures: response.data.procedures,
						start_date: response.data.start_date,
						end_date: response.data.end_date,
						location: response.data.location
					})
				} else {
					this.setState({
						appointment: "",
						location: "",
						start_date: "",
						end_date: "",
						appointment: "",
						doctor: "",
						patient: ""

					})
				}
			})

	}



	handleInput = (name, e) => {
		this.setState({
			appointment: { ...this.state.appointment, [name]: e.target.value },
		});
	};

	handleDoctorInput = (e) => {
		let value = e.target.value;
		if (value) {
			let abc = [];
			let b = this.props.doctors
				? this.props.doctors.map((item, i) =>
					item.id == parseInt(value)
						? item.procedures.map((p, j) => abc.push({ value: p, label: p }))
						: null
				)
				: null;
			this.setState({
				doctor: value,
				procedureDisable: abc.length ? true : false,
				options: abc,
			});
		} else {
			this.setState({
				doctor: "",
				procedureDisable: false,
				options: [],
			});
		}
	};

	handleInputAllDays = () => {
		this.setState({
			all_day: !this.state.all_day,
		});
	};

	onChange = (opt) => {
		const allOptionsSelected = opt
			? opt.length === this.state.options.length
			: false;
		let valueSelect = opt ? opt.map((item, i) => item["value"]) : [];
		this.setState({
			checked: allOptionsSelected ? true : false,
			value: opt,
			procedures: valueSelect,
		});
	};



	handleCancle = () => {
		this.setState({
			appointment: {
				location: "",
				patient: "",
				notes: "",
				status: "",
				start_date: "",
				end_date: "",
			},
			procedures: [],
			value: [],
			options: [],

			doctor: "",
			all_day: false,
			procedureDisable: false,
			all_day: false,
			fieldsError: "",
		});
	};

	render() {
		const allDoctor = this.props.doctors;
		const allPatient = this.props.patients;
		const allClinic = this.props.clinics;
		const allSpeciality = this.props.speciality;
		const options = this.state.procedures;
		const location = this.state.location;
		// console.log('all appointment',allClinic,allSpeciality,options,allPatient,allDoctor)
		// console.log('all clinic***',allClinic)
		// console.log(" editappointment props", this.props);
		console.log('location', this.state)
		return (
			<div>
				<div>
					<div className="content">
						<Row>
							<Col xs={12} md={12}>
								<div className="page-title">
									<div className="float-left">
										<h1 className="title">Update Appointment</h1>
									</div>
								</div>

								<div className="row margin-0">
									<div className="col-12">
										<section className="box ">
											<header className="panel_header">
												<h2 className="title float-left">Basic Info</h2>
											</header>
											<div className="content-body">
												<div className="row">
													<div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
														<form>
															<div className="form-row">
															
																<div className="form-group col-md-12">
																	<label
																		htmlFor="speciality-ember1319"
																		className="control-label ember-view"
																	>
																		Patient
                          </label>
																	<div id="ember1329" className="ember-view">
																		<select
																			id="speciality-ember1319"
																			class="form-control"
																			value={this.state.appointment.patient}
																			onChange={(e) => this.handleInput("patient", e)}
																		>
																			<option value="">Select...</option>
																			{
																				allPatient ? allPatient.map((item, i) =>
																					<option value={item.id} >
																						{
																							item.middle_name ?
																								item.first_name + " " + item.middle_name + " " + item.last_name :
																								item.first_name + " " + item.last_name
																						}
																					</option>
																				) : null
																			}
																		</select>
																	</div>
																</div>
															
																<div className="form-group col-md-12">
																	<label
																		htmlFor="speciality-ember1319"
																		className="control-label ember-view"
																	>
																		Doctor
                          </label>
																	<div id="ember1329" className="ember-view">
																		<select
																			id="speciality-ember1319"
																			class="form-control"
																			value={this.state.doctor}
																			onChange={(e) => this.handleDoctorInput(e)}
																		>
																			<option value="">Select...</option>
																			{
																				allDoctor ? allDoctor.map((item, i) =>
																					<option value={item.id} >
																						{
																							item.middle_name ?
																								item.first_name + " " + item.middle_name + " " + item.last_name :
																								item.first_name + " " + item.last_name
																						}
																					</option>
																				) : null
																			}

																		</select>
																	</div>
																</div>
														
																<div className="form-group col-md-12">
																	<label htmlFor="inputname4">Start Date</label>
																	<input
																		type="date"
																		className="form-control"
																		id="inputname4"
																		placeholder=""
																		value={this.state.appointment.start_date}
																		onChange={(e) => this.handleInput("start_date", e)}
																	/>
																</div>

																<div className="form-group col-md-12">
																	<label
																		htmlFor="speciality-ember1319"
																		className="control-label ember-view"
																	>
																		Speciality
                          </label>
																	<div id="ember1329" className="ember-view">
																		<select
																			className="form-control"
																			id="status-ember1273"
																			value={this.state.appointment.status}
																			onChange={(e) => this.handleInput("status", e)}
																		>
																			<option value="">Select...</option>
																			<option value="attendent">Attendent</option>
																			<option value="scheduled">Scheduled</option>
																			<option value="missed">Missed</option>
																			<option value="cancelled">Cancelled</option>
																		</select>

																	</div>
																</div>
	
																<div className="form-group col-md-12">
																	<label htmlFor="inputname4">All Days</label>
																	<input
																		type="text"
																		className="form-control"
																		id="inputname4"
																		placeholder=""
																		onChange={this.handleInputAllDays}
																		checked={this.state.all_day}
																	/>
																</div>

																<div className="form-group col-md-12">
																	<label
																		htmlFor="speciality-ember1319"
																		className="control-label ember-view"
																	>
																		Clinic
                          </label>
																	<div id="ember1329" className="ember-view">
																		<select
																			id="speciality-ember1319"
																			class="form-control"
																			value={this.state.appointment.location}
																			onChange={(e) => this.handleInput("location", e)}

																		>

																			<option value="item">Select...</option>
																															{/* {
																				allClinic.length ? allClinic.map((item, i) =>
																				<option value={item.id} >{item.clinic_name}</option>
																					) : null
																					} */}

																		</select>
																	</div>
																</div>
															
																<div className="form-group col-md-12">
																	<label
																		htmlFor="speciality-ember1319"
																		className="control-label ember-view"
																	>
																		Procedures
                          </label>
																	<div id="ember1329" className="ember-view">
																		<Select
																			isMulti
																			onChange={this.onChange}
																			options={options}
																			value={this.state.value}
																		/>

																	</div>
																</div>
															
																<div className="form-group col-md-12">
																	<label htmlFor="inputname4">End Date</label>
																	<input
																		type="date"
																		className="form-control"
																		id="inputname4"
																		value={this.state.appointment.end_date}
																		onChange={(e) => this.handleInput("end_date", e)}

																	/>
																</div>
																
																<div id="ember1491" className="col-sm-12">
																	<label
																		htmlFor="address-ember1491"
																		id="ember1493"
																		className="control-label ember-view"
																	>
																		Notes
                              </label>
                              <div id="ember1503" className="ember-view">
                              <textarea
                                rows="3"
                               id="address-ember1491"
                               className="form-control"
                                placeholder="Write some notes here ..."
                               value={this.state.appointment.notes}
                               onChange={(e) => this.handleInput("notes", e)}
                                />
                              </div>
                              
                              <span className="form-control-feedback">
                                {this.state.fieldsError}
                              </span>
                            </div>
                          </div>
        
                        <button className="btn btn-primary m-4" onClick={this.handleSubmit}>Submit</button>
                    </form>
                  </div>
                </div>
              </div>
            </section>
          </div> 
        </div>
      </Col>
    </Row>
  </div>
</div>
  </div>
    );
  }
}
const mapStateToProps = (state) => {
	return {
		clinics: state.clinic.allClinic,
		patients: state.patient.allPatients,
		doctors: state.doctor.allDoctor,
		speciality: state.speciality.allSpeciality,
	};
};
export default connect(mapStateToProps)(UpdateAppointment);
