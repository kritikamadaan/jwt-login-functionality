import React, { Component } from "react";
import { connect } from "react-redux";
import Select, { NonceProvider } from "react-select";
import { Row, Col, Label, Input } from "reactstrap";

export class AddAppointment extends Component {
	state = {
		appointment: {
			location: "",
			patient: "",
			notes: "",
			status: "",
			start_date: "",
			end_date: ""
		},

		procedures: [],
		value: [],
		options: [],

		doctor: "",
		all_day: false,
		procedureDisable: false,
		token: window.localStorage.getItem("token"),
		fieldsError: ""

	};

	handleInput = (name, e) => {
		this.setState({
			appointment: { ...this.state.appointment, [name]: e.target.value }
		});
	}

	handleDoctorInput = (e) => {
		let value = e.target.value
		if (value) {
			let abc = []
			let b = this.props.doctors ? this.props.doctors.map((item, i) =>
				item.id == parseInt(value) ? item.procedures.map((p, j) =>
					abc.push({ value: p, label: p })
				) : null
			) : null
			this.setState({
				doctor: value,
				procedureDisable: abc.length ? true : false,
				options: abc
			})
		} else {
			this.setState({
				doctor: '',
				procedureDisable: false,
				options: []
			})
		}
	}


	handleInputAllDays = () => {
		this.setState({
			all_day: !this.state.all_day
		});
	}

	onChange = (opt) => {

		const allOptionsSelected = opt ? opt.length === this.state.options.length : false
		let valueSelect = opt ?
			opt.map((item, i) => (
				item['value']
			))
			: []
		this.setState({
			checked: allOptionsSelected ? true : false,
			value: opt,
			procedures: valueSelect
		});
	};


	handleSubmit = () => {
		let data = this.state.appointment;
		data["all_day"] = this.state.all_day;
		data["doctor"] = this.state.doctor;
		data["procedures"] = this.state.procedures;
		if (
			data["doctor"] &&
			data["procedures"] &&
			data["patient"] &&
			data["patient"] &&
			data["status"] &&
			data["start_date"] &&
			data["end_date"]
		) {
			
			fetch("/appointments/create/", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + this.state.token,
				},
				body: JSON.stringify(data),
			})
				.then((res) => res.json())
				.then((response) => {
					if (response.status) {
						this.props.dispatch({
							type: "ADD_NEW_APPOINTMENT",
							appointment: response.data,
						});
						this.setState({
							appointment: {
								location: "",
								patient: "",
								notes: "",
								status: "",
								start_date: "",
								end_date: "",
							},
							procedures: [],
							value: [],
							options: [],

							doctor: "",
							all_day: false,
							procedureDisable: false,
							all_day: false,
							fieldsError: "",
						});
					}
				});
		} else {
			this.setState({
				fieldsError: "All '*' fields are required",
			});
		}
	};

	handleCancle = () => {
		this.setState({
			appointment: {
				location: "",
				patient: "",
				notes: "",
				status: "",
				start_date: "",
				end_date: "",
			},
			procedures: [],
			value: [],
			options: [],

			doctor: "",
			all_day: false,
			procedureDisable: false,
			all_day: false,
			fieldsError: "",
		});
	};

	render() {
		const allDoctor = this.props.doctors;
		const allPatient = this.props.patients;
		const allClinic = this.props.clinics;
		const allSpeciality = this.props.speciality;
		const options = this.state.options;
		return (
			<div>
				<div>
					<div className="content">
						<Row>
							<Col xs={12} md={12}>
								<div className="page-title">
									<div className="float-left">
										<h1 className="title">Add Appointment</h1>
									</div>
								</div>

								<div className="row margin-0">
									<div className="col-12">
										<section className="box ">
											<header className="panel_header">
												<h2 className="title float-left">Basic Info</h2>
											</header>
											<div className="content-body">
												<div className="row">
													<div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
														<form>
															<div className="form-row">
																<div className="form-group col-md-8">
																	<label htmlFor="speciality-ember1319" >Patient</label>
																	<select
																		id="speciality-ember1319"
																		class="form-control"
																		value={this.state.appointment.patient}
																		onChange={(e) => this.handleInput("patient", e)}
																	>
																		<option value="">Select...</option>
																		{
																			allPatient ? allPatient.map((item, i) =>
																				<option value={item.id} >
																					{
																						item.middle_name ?
																							item.first_name + " " + item.middle_name + " " + item.last_name :
																							item.first_name + " " + item.last_name
																					}
																				</option>
																			) : null
																		}
																	</select>

																</div>

																<div className="form-group col-md-8">
																	<label htmlFor="speciality-ember1">Doctor</label>
																	<select
																		id="speciality-ember1"
																		class="form-control"
																		value={this.state.doctor}
																		onChange={(e) => this.handleDoctorInput(e)}
																	>
																		<option value="">Select...</option>
																		{
																			allDoctor ? allDoctor.map((item, i) =>
																				<option value={item.id} >
																					{
																						item.middle_name ?
																							item.first_name + " " + item.middle_name + " " + item.last_name :
																							item.first_name + " " + item.last_name
																					}
																				</option>
																			) : null
																		}

																	</select>
																</div>
																<div className="form-group col-md-8">
																	<label htmlFor="speciality-ember2">Status</label>
																	<select
																		className="form-control"
																		id="speciality-ember2"
																		value={this.state.appointment.status}
																		onChange={(e) => this.handleInput("status", e)}
																	>
																		<option value="">Select...</option>
																		<option value="attendent">Attendent</option>
																		<option value="scheduled">Scheduled</option>
																		<option value="missed">Missed</option>
																		<option value="cancelled">Cancelled</option>
																	</select>

																</div>
																<div className="form-group col-md-8">
																	<label htmlFor="speciality-ember4">Procedures</label>
																	<Select
																		isMulti
																		onChange={this.onChange}
																		options={options}
																		value={this.state.value}
																	/>
																</div>

																<div className="form-group col-md-8">
																	<label htmlFor="speciality-ember3">Clinic</label>
																	<select
																		id="speciality-ember3"
																		class="form-control"
																		value={this.state.appointment.location}
																		onChange={(e) => this.handleInput("location", e)}

																	>

																		<option value="">Select...</option>
																		{
																			allClinic.length ? allClinic.map((item, i) =>
																				<option value={item.id} >{item.clinic_name}</option>
																			) : null
																		}

																	</select>
																</div>
															</div>
															<div className="form-row">
																<div className="form-group col-md-4">
																	<label htmlFor="inputname4">Start Date</label>
																	<input
																		type="date"
																		className="form-control"
																		id="inputname4"
																		placeholder=""
																		value={this.state.appointment.start_date}
																		onChange={(e) => this.handleInput("start_date", e)}
																	/>
																</div>
																<div className="form-group col-md-4">
																	<label htmlFor="inputname7">End Date</label>
																	<input
																		type="date"
																		className="form-control"
																		id="inputname4"
																		value={this.state.appointment.end_date}
																		onChange={(e) => this.handleInput("end_date", e)}
																	/>
																</div>
															</div>
															<div className="form-row">
																<div className="form-group col-md-8">
																	<label htmlFor="notes">All Days</label>
																	<input
																		type="checkbox"
																		className="form-control"
																		id="notes"
																		onChange={this.handleInputAllDays}
																		checked={this.state.all_day}
																	/>
																</div>
															</div>
															<div className="form-row">
																<div className="form-group col-md-8">
																	<Label htmlFor="exampleText">Notes</Label>
																	<Input
																		type="textarea"
																		name="text"
																		id="exampleText"
																		value={this.state.appointment.notes}
																		onChange={(e) => this.handleInput("notes", e)}
																	/>
																</div>
															</div>
															<button
															className="btn btn-primary m-4"
															onClick={this.handleSubmit}
														    >Submit
														</button>
														</form>	
													</div>
												</div>
											</div>
										</section>
									</div>
								</div>
							</Col>
						</Row>
					</div>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		clinics: state.clinic.allClinic,
		patients: state.patient.allPatients,
		doctors: state.doctor.allDoctor,
		speciality: state.speciality.allSpeciality,
	};
};
export default connect(mapStateToProps)(AddAppointment);




