import React, { Component } from "react";
import { connect } from "react-redux";
import {  Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { withRouter } from "react-router-dom";

import Select from "react-select";

class EditingProcedures extends Component {
	state = {
		options: [],
		token: window.localStorage.getItem("token"),
		multiValue: [],
		speciality: "",
		value: [],
		selectDisable: false,
		specialityError: "",
		procedureError: "",
		modal: false,
	};
	toggle = () => {
		this.setState({
			modal: !this.state.modal,
		});
	};
	closeModel = () => {
		this.setState({
			modal: false,
		});
	};

	componentDidMount() {
		const token = this.state.token
		fetch(`/doctors/procedures-details/${this.props.id}/`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token
			},
		})
			.then(res => res.json())
			.then(response => {
				if (response.status) {
					let procedure=response.data.procedures
					let option = []
					let b = procedure.map((item, i) =>
						option.push({ value: item, label: item })
					)
					this.setState({
						speciality: response.data.speciality.id,
						value: option
					})
				}
				else {
					this.setState({
						speciality: "",
						value: []
					})
				}


			})

	}

	handleInput = (e) => {
		let value = e.target.value
		if (value) {
			let abc = []
			let b = this.props.procedures ? this.props.procedures.map((item, i) =>
				item.speciality.id == parseInt(value) ? item.procedures.map((p, j) =>
					abc.push({ value: p, label: p })
				) : null
			) : null
			this.setState({
				speciality: e.target.value,
				procedureDisable: abc.length ? true : false,
				options: abc,
				value: []
			})
		} else {
			this.setState({
				speciality: '',
				procedureDisable: false,
				options: []
			})
		}
	}
	onChangeProcedure = (opt) => {

		const allOptionsSelected = opt ? opt.length === this.state.options.length : false
		let valueSelect = opt ?
			opt.map((item, i) => (
				item['value']
			))
			: []
		this.setState({
			checked: allOptionsSelected ? true : false,
			value: opt,
			procedures: valueSelect
		});
	};

	render() {
		const allSpeciality = this.props.speciality;
		const modal = this.props.modal;

		return (
			<div>

				<Modal isOpen={modal} toggle={this.props.openHandler} className="">
					<ModalHeader>
						<h5 className="modal-title">Edit Procedure</h5>
					</ModalHeader>
					<ModalBody>
						<div className="modal-body">
							<form>
								<div className="form-row">
									<div className="form-group col-md-12">

										<label htmlFor="inputname4">Speciality</label>
										<select
											className="form-control"
											id="inputPassword3"
											onChange={(e) => this.handleInput(e)}
											value={this.state.speciality}
											required
										>
											<option value="">Choose...</option>
											{allSpeciality
												? allSpeciality.map((item, i) => (
													<option value={item.id}>
														{item.speciality_name}
													</option>
												))
												: null}
										</select>

									</div>
									<div className="form-group col-md-12">
										<label htmlFor="inputname5">Procedures</label>
										<Select
											isMulti
											onChange={this.onChangeProcedure}
											options={this.state.options}
											value={this.state.value}
											isDisabled={!this.state.procedureDisable}
											required="true"
										/>
										<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
											{this.state.procedureError}
										</div>
									</div>
								</div>
							</form>
						</div>
					</ModalBody>
					<ModalFooter>
						<div className="modal-footer">
							<button
								type="button"
								className="btn btn-sm btn-secondary"
								data-dismiss="modal"
								onClick={this.props.closeModal}
							>
								Close
			             	</button>
							<button
								type="button"
								className="btn btn-sm btn-primary"
								onClick={this.handleSubmit}

							>
								Submit
			              </button>
						</div>
					</ModalFooter>
				</Modal>
			</div>

		);
	}
}
const mapStateToProps = (state) => {
	return {
		speciality: state.speciality.allSpeciality,
		procedures: state.procedure.allProcedures
	};
};
export default connect(mapStateToProps)(withRouter(EditingProcedures));
