import React from 'react';
import {

    Row, Col,
} from 'reactstrap';

import {

} from 'components';
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom';

var BASEDIR = process.env.REACT_APP_BASEDIR;
var IMGDIR = process.env.REACT_APP_IMGDIR;

class ProcedureProfile extends React.Component {

    render() {
        const allProcedure = this.props.procedure
        return (
            <div>
                <div className="content">
                    <Row>
                        <Col xs={12} md={12}>
                            <div className="page-title">
                                <div className="float-left">
                                    <h1 className="title">Procedure Profile</h1>
                                </div>
                            </div>
                            {
                                allProcedure ? allProcedure.map((item, i) => (
                                    item.id === parseInt(this.props.id) ?
                                        <div className="col-xl-12">
                                            <section className="box profile-page">
                                                <div className="content-body">
                                                    <div className="col-12">
                                                        <div className="row uprofile">
                                                            <div className="uprofile-image col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                                                                <img alt="" src={IMGDIR + "/images/hospital/doctors/doctor.jpeg"} className="img-fluid" /> 
                                                            </div>
                                                            <div className="uprofile-name col-xl-10 col-lg-9 col-md-9 col-sm-8 col-12">
                                                                <h3 className="uprofile-owner">
                                                                    <p>
                                                                        {item.procedure_name}
                                                                    </p>
                                                                </h3>
                                                                <NavLink
                                                                    className="btn btn-primary btn-sm profile-btn"
                                                                    to={BASEDIR + `/dashboard/edit/${"procedure"}/${item.id}`}
                                                                    title="edit profile"
                                                                >Edit
                                                                </NavLink>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="clearfix"></div>
                                                        <hr />
                                                        <h4>Procedures:</h4>
                                                        <ul>
                                                            <ul>
                                                                {item.procedures.map((item,i)=>(
                                                                   <li>Procedure:{" "}{item}</li>  
                                                                ))}
                                                            </ul>
                                                        </ul>
                                                        <div className="clearfix"></div>

                                                    </div>
                                                    <div className="col-12">
                                                        <div className="clearfix"></div>
                                                        <hr />
                                                        <h4>Speciality:</h4>
                                                        <ul>
                                                            <ul>
                                                                <li>{item.speciality.speciality_name}</li>
                                                            </ul>
                                                        </ul>
                                                        <div className="clearfix"></div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div> :null
                                )) : null
                            }
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        procedure: state.procedure.allProcedures
    };
};
export default connect(mapStateToProps)(ProcedureProfile);
