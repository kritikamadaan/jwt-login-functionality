import React, { Component } from "react";
import { connect } from "react-redux";
import {  Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

import Select from "react-select";

class AddProcedures extends Component {
  state = {
    options: [
      { value: "Laser Proctology", label: "Laser proctology" },
      { value: "Cosmetic Gynaecology", label: "Cosmetic Gynaecology" },
      { value: "Laser Lircumcision", label: "Laser Circumcision" },
      { value: "Laser Hydrocele", label: "Laser Hydrocele" },
      { value: "Laser Frenuloplasty", label: "Laser Frenuloplasty" },
    ],

    token: window.localStorage.getItem("token"),
    multiValue: [],
    speciality: "",
    value: [],
    selectDisable: false,
    specialityError: "",
    procedureError: "",
    modal: false,
  };
  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };
  closeModel = () => {
    this.setState({
      modal: false,
    });
  };
  handleInput = (e) => {
    let value = e.target.value;
    if (value) {
      this.setState({
        speciality: value,
        selectDisable: true,
        specialityError: "",
        procedureError: "",
      });
    }
  };

  onChange = (opt) => {
    const allOptionsSelected = opt
      ? opt.length === this.state.options.length
      : false;
    let valueSelect = opt ? opt.map((item, i) => item["value"]) : [];
    this.setState({
      checked: allOptionsSelected ? true : false,
      value: opt,
      multiValue: valueSelect,
      specialityError: "",
      procedureError: "",
    });
  };

  handleSubmit = () => {
    const data = {
      speciality: this.state.speciality,
      procedures: this.state.multiValue,
    };
    if (data["speciality"] && data["procedures"].length) {
      fetch("/doctors/procedures/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.state.token,
        },
        body: JSON.stringify(data),
      })
        .then((res) => res.json())
        .then((response) => {
          if (response.status) {
            this.props.dispatch({
              type: "ADD_NEW_PROCEDURES",
              procedure: response.data,
            });
            this.setState({
              multiValue: [],
              speciality: "",
              value: [],
              selectDisable: false,
              specialityError: "",
              procedureError: "",
            });
          }
        });
    } else if (!data["speciality"]) {
      this.setState({
        specialityError: "This field are required",
        procedureError: "",
      });
    } else {
      this.setState({
        specialityError: "",
        procedureError: "This field are requied",
      });
    }
  };

  closeModel = () => {
    this.setState({
      multiValue: [],
      speciality: " ",
      value: [],
      selectDisable: false,
      specialityError: "",
      procedureError: "",
    });
  };

  render() {
    const options = this.state.options;
    const allSpeciality = this.props.speciality;
    const modal = this.props.modal;
    const toggle=this.props.toggle;
    return (
      <div>
        <Modal isOpen={modal} toggle={toggle} className="">
          <ModalHeader toggle={toggle}>
            <h5 className="modal-title">Add New Procedures</h5>
          </ModalHeader>
          <ModalBody>
            <form>
              <div className="form-group row">
                <label
                  htmlFor="inputPassword3"
                  className="col-sm-3 col-form-label"
                >
                  Speciality
                </label>
                <div className="col-sm-9">
                  <select
                    className="form-control"
                    id="inputPassword3"
                    onChange={(e) => this.handleInput(e)}
                    value={this.state.speciality}
                  >
                    <option value="">Choose...</option>
                    {allSpeciality
                      ? allSpeciality.map((item, i) => (
                          <option value={item.id}>
                            {item.speciality_name}
                          </option>
                        ))
                      : null}
                  </select>
                  <div
                    className="check-point-name-qx01 m-0 p-0"
                    style={{ color: "red" }}
                  >
                    {this.state.specialityError}
                  </div>
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="inputPassword3"
                  className="col-sm-3 col-form-label"
                >
                  Procedures
                </label>
                <div className="col-sm-9">
                  <Select
                    isMulti
                    onChange={this.onChange}
                    options={options}
                    value={this.state.value}
                    isDisabled={!this.state.selectDisable}
                  />
                  <div
                    className="check-point-name-qx01 m-0 p-0"
                    style={{ color: "red" }}
                  >
                    {this.state.procedureError}
                  </div>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-sm btn-secondary"
              data-dismiss="modal"
              onClick={this.props.closeModel}
            >
              Close
            </button>
            <button
              type="button"
              className="btn btn-sm btn-primary"
              onClick={this.handleSubmit}
            >
              Submit
            </button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    speciality: state.speciality.allSpeciality,
  };
};
export default connect(mapStateToProps)(AddProcedures);
