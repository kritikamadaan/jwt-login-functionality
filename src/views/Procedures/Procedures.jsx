import React from 'react';
import {
	Button,
	Row, Col,
} from 'reactstrap';

import {
	Procedurelist
} from 'components';
import AddProcedures from './AddProcedures';


class Doctor extends React.Component {
	state={
		 //modal states
		 modal: false,
	}
 

toggle = () => {
  this.setState({
	modal: !this.state.modal,
  });
};
closeModel = () => {
  this.setState({
	modal: false,
  });
};
	render() {
		return (
			<div>
				<div className="content">
					<Row>
						<Col xs={12} md={12}>
							<div className="page-title">
								<div className="float-left">
									<h1 className="title">Procedure</h1>
								</div>
							</div>
							<div className="col-xl-12">
								<section className="box ">
									<header className="panel_header">
										<h2 className="title float-left">All Procedures</h2>
										<span className="btn btn-primary btn-sm-profile-btn float-right m-4" onClick={this.toggle}>
										Add New  Procedure
										</span>
									</header>
									<div className="content-body">
										<div className="row">
											<div className="col-12">
												<Procedurelist />
											</div>
										</div>
									</div>
								</section>
							</div>
						</Col>
						{
						this.state.modal 
						? 	
						<AddProcedures
						modal={this.state.modal}
						toggle={this.toggle}
						closeModel={this.closeModel}
						/>
						: null
						}
						
					</Row>
				</div>
			</div>
		);
	}
}

export default Doctor;
