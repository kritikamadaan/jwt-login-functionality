import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from "reactstrap";
import { isThisTypeNode } from "typescript";

export class EditClinic extends Component {
	state = {
		newClinic: {
			clinic_name: "",
			clinic_location: "",
			longitude: "",
			latitude: "",
		},
		clinicError: "",
		locationError: "",
		token: window.localStorage.getItem("token"),
		modal: false,
	};

	handleInput = (name, e) => {
		this.setState({
			newClinic: { ...this.state.newClinic, [name]: e.target.value },
			clinicError: "",
			locationError: "",
		});
	};
	componentDidMount() {

		const token = this.state.token
		fetch(`/doctors/clinic/details/${this.props.id}/`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token
			},
		})
			.then(res => res.json())
			.then(response => {
				 console.log('editclinic',response)
				if (response.status) {
					this.setState({
						newClinic: response.data
					})
				}else{
					this.setState({
						newClinic: {
							clinic_name: "",
							clinic_location: "",
							longitude: "",
							latitude: "",
						},
					})
				}
			})

	}

	render() {
		const modal = this.state.modal;
		return (
			<div>
				<div className="content">
					<Row>
						<Col xs={12} md={12}>
							<div className="page-title">
								<div className="float-left">
									<h1 className="title">Edit Clinic</h1>
								</div>
							</div>
							<div className="row margin-0">
								<div className="col-12">
									<section className="box ">
										<header className="panel_header">
											<h2 className="title float-left">Basic Info</h2>

										</header>
										<div className="content-body">
											<div className="row">
												<div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
													<form>
														<div className="form-row">
															<div className="form-group col-md-8">
																<label htmlFor="inputname4">Clinic  editName</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname4"
																	placeholder="Enter Clinic name"
																	value={this.state.newClinic.clinic_name}
																	onChange={(e) => this.handleInput("clinic_name", e)}
																	required
																/>
															</div>
															<div className="form-group col-md-8">
																<label htmlFor="inputname5">Location</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname5"
																	placeholder="Enter Location"
																	value={this.state.newClinic.clinic_location}
																	onChange={(e) => this.handleInput("clinic_location", e)}
																	required
																/>
															</div>
															<div className="form-group col-md-8">
																<label htmlFor="inputname6">Longitude</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname6"
																	placeholder="Enter last name"
																	value={this.state.newClinic.longitude}
																	onChange={(e) => this.handleInput("longitude", e)}
																	required
																/>
															</div>
															<div className="form-group col-md-8">
																<label htmlFor="inputname6">Latitude</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname6"
																	placeholder="Enter last name"
																	value={this.state.newClinic.latitude}
																	onChange={(e) => this.handleInput("latitude", e)}
																	required
																/>
															</div>
														</div>
													</form>
													{/* <button
														type="submit"
														className="btn 	btn-danger"
														// style={{float:"right"}}
														onClick={this.handleCancle}
													>Cancle
													</button> */}
													<button
														type="submit"
														className="btn btn-primary"
														// style={{float:"right"}}
														onClick={this.handleSubmit}
													>Save
													</button>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</Col>
					</Row>
				</div>
			</div>
		);
	}
}

export default connect()(EditClinic);





