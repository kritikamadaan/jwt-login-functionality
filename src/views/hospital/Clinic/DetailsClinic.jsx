import React from 'react';
import {

    Row, Col,
} from 'reactstrap';

import {

} from 'components';
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom';

var BASEDIR = process.env.REACT_APP_BASEDIR;
var IMGDIR = process.env.REACT_APP_IMGDIR;

class DetailsClinic extends React.Component {

    render() {
        const allClinic = this.props.clinic
        console.log('all clinic',allClinic)
        return (
            <div>
                <div className="content">
                    <Row>
                        <Col xs={12} md={12}>

                            <div className="page-title">
                                <div className="float-left">
                                    <h1 className="title">Doctor Profile</h1>
                                </div>
                            </div>
                            {
                                allClinic ? allClinic.map((item, i) => (

                                    item.id === parseInt(this.props.id) ?
                                        <div className="col-xl-12">
                                            <section className="box profile-page">
                                                <div className="content-body">
                                                    <div className="col-12">
                                                        <div className="row uprofile">
                                                            <div className="uprofile-image col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                                                            <img alt="" src={IMGDIR + "/images/hospital/doctors/doctor.jpeg"} className="img-fluid" />
                                                            </div>
                                                            <div className="uprofile-name col-xl-10 col-lg-9 col-md-9 col-sm-8 col-12">
                                                                <h3 className="uprofile-owner">
                                                                    <p>
                                                                        {item.clinic_name}
                                                                    </p>
                                                                </h3>
                                                                <NavLink
                                                                    className="btn btn-primary btn-sm profile-btn"
                                                                    to={BASEDIR + `/dashboard/edit/${"clinic"}/${item.id}`}
                                                                    title="edit profile"
                                                                >Edit
                                                                </NavLink>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-12">

                                                        <div className="clearfix"></div>
                                                        <hr />
                                                        <h4>Address:</h4>
                                                        <ul>
                                                            <ul>
                                                                <li>Location: {item.clinic_location}</li>
                                                                <li>Longitude: {item.longitude}</li>
                                                                <li>Latitude: {item.latitude}</li>
                                                            </ul>
                                                        </ul>
                                                        <div className="clearfix"></div>


                                                    </div>
                                                </div>
                                            </section>
                                        </div> :null
                                )) : null
                            }
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        clinic: state.clinic.allClinic
    };
};
export default connect(mapStateToProps)(DetailsClinic);
// export default DoctorProfile;
