import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
} from "reactstrap";

export class UpdateClinic extends Component {
  state = {
    newClinic: {
      clinic_name: "",
      clinic_location: "",
      longitude: "",
      latitude: "",
    },
    clinicError: "",
    locationError: "",
    token: window.localStorage.getItem("token"),
    modal: false,
  };

  handleInput = (name, e) => {
    this.setState({
      newClinic: { ...this.state.newClinic, [name]: e.target.value },
      clinicError: "",
      locationError: "",
    });
  };

  handleSubmit = () => {
    const data = this.state.newClinic;

    if (data["clinic_name"] && data["clinic_location"]) {
      fetch("/doctors/clinic/create-and-list/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.state.token,
        },
        body: JSON.stringify(data),
      })
        .then((res) => res.json())
        .then((response) => {
          if (response.status) {
            this.props.dispatch({
              type: "ADD_NEW_CLINIC",
              clinic: response.data,
            });
            this.setState({
              newClinic: {
                clinic_name: "",
                clinic_location: "",
                longitude: "",
                latitude: "",
                clinicError: "",
                locationError: "",
              },
            });
          }
        });
    } else if (!data["clinic_name"]) {
      this.setState({
        clinicError: "This fields are required",
        locationError: "",
      });
    } else {
      this.setState({
        clinicError: "",
        locationError: "This fields are required",
      });
    }
  };

  closeModel = () => {
    this.setState({
      newClinic: {
        clinic_name: "",
        clinic_location: "",
        longitude: "",
        latitude: "",
        clinicError: "",
        locationError: "",
        modal: false,
      },
    });
  };
  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  render() {
    return (
        <div>
        <div className="content">
            <Row>
                <Col xs={12} md={12}>
                    <div className="page-title">
                        <div className="float-left">
                            <h1 className="title">Edit Clinic</h1>
                        </div>
                    </div>
                    <div className="row margin-0">
                        <div className="col-12">
                            <section className="box ">
                                <header className="panel_header">
                                    <h2 className="title float-left">Basic Info</h2>

                                </header>
                                <div className="content-body">
                                    <div className="row">
                                        <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
                                            <form>
                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="inputname4">Clinic  Name</label>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="inputname4"
                                                            placeholder="Enter Clinic name"
                                                            value={this.state.newClinic.clinic_name}
                                                            onChange={(e) => this.handleInput("clinic_name", e)}
                                                            required
                                                        />
                                                    </div>
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="inputname5">Location</label>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="inputname5"
                                                            placeholder="Enter Location"
                                                            value={this.state.newClinic.clinic_location}
                                                            onChange={(e) => this.handleInput("clinic_location", e)}
                                                            required
                                                        />
                                                    </div>
                      
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="inputname6">Longitude</label>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="inputname6"
                                                            placeholder="Enter last name"
                                                            value={this.state.newClinic.longitude}
                                                            onChange={(e) => this.handleInput("longitude", e)}
                                                            required
                                                        />
                                                    </div>
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="inputname6">Latitude</label>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="inputname6"
                                                            placeholder="Enter last name"
                                                            value={this.state.newClinic.latitude}
                                                            onChange={(e) => this.handleInput("latitude", e)}
                                                            required
                                                        />
                                                    </div>
                                                </div>
                                                <button type="submit" className="btn btn-primary">Save</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    </div>
    );
  }
}

export default connect()(UpdateClinic);
