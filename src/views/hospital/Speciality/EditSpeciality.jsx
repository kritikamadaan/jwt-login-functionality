import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";


export class EditSpeciality extends Component {
	state = {
		token: window.localStorage.getItem("token"),
		speciality: "",
		specialityError: "",
		modal: false,
	};
	handleInput = (e) => {
		this.setState({
			speciality: e.target.value,
			specialityError: "",
		});
	};

	componentDidMount() {
		const token = this.state.token
		fetch(`/doctors/speciality-details/${this.props.id}/`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token
			},
		})
			.then(res => res.json())
			.then(response => {
				if (response.status) {
					this.setState({
						speciality: response.data.speciality_name
					})
				}
				else {
					this.setState({
						speciality: "",
					})
				}

			})
	}


	render() {
		const modal = this.props.modal;
		const options = this.state.options;
		const allSpeciality = this.props.speciality;
		return (
			<div>
				<Modal isOpen={modal} toggle={this.props.openHandler} className="">
					<ModalHeader toggle={this.toggle}>
						<h5 className="modal-title">Edit Speciality</h5>
					</ModalHeader>
					<ModalBody>
						<form>
							<div className="form-row">
								<div className="form-group col-md-12">
									<label htmlFor="inputname4">Speciality Name</label>
									<input
										type="text"
										className="form-control"
										id="inputname4"
										placeholder="Enter speciality name"
										value={this.state.speciality}
										onChange={(e) => this.handleInput(e)}
									/>
									<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
										{this.state.specialityError}
									</div>
								</div>
							</div>
						</form>
					</ModalBody>
					<ModalFooter>
						<button
							type="button"
							className="btn btn-sm btn-secondary"
							data-dismiss="modal"
							onClick={this.props.closeModal}
						>
							Close
            			</button>
						<button
							type="button"
							className="btn btn-sm btn-primary"
							onClick={this.handleSubmit}
						>
							Submit
            			</button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		speciality: state.speciality.allSpeciality,
	};
};
export default connect(mapStateToProps)(EditSpeciality);
