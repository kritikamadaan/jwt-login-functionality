import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";


export class UpdateSpeciality extends Component {
	state = {
		token: window.localStorage.getItem("token"),
		speciality: "",
		specialityError: "",
		modal: false,
	};

	handleInput = (e) => {
		this.setState({
			speciality: e.target.value,
			specialityError: "",
		});
	};

	handleSubmit = () => {
		const data = {
			speciality_name: this.state.speciality,
		};

		if (data["speciality_name"]) {
			fetch("/doctors/speciality/", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + this.state.token,
				},
				body: JSON.stringify(data),
			})
				.then((res) => res.json())
				.then((response) => {
					if (response.status) {
						this.props.dispatch({
							type: "NEW_SPECIALITY",
							Speciality: response.data,
						});
						this.setState({
							speciality: "",
							specialityError: "",
						});
					}
				});
		} else {
			this.setState({
				specialityError: "This field are required",
			});
		}
	};

	closeModel = () => {
		this.setState({
			speciality: "",
			specialityError: "",
		});
	};
	toggle = () => {
		this.setState({
			modal: !this.state.modal,
		});
	};
	modalClose = () => {
		this.setState({
			modal: false,
		});
	};

	render() {
		const modal = this.state.modal;
		const options = this.state.options;
		const allSpeciality = this.props.speciality;
		return (
			<div>
				<h1>Add</h1>
				<Button color="danger" onClick={this.toggle}>
					button
                </Button>
				<Modal isOpen={modal} toggle={this.toggle} className="">
					<ModalHeader toggle={this.toggle}>
						<h5 className="modal-title">Add New Speciality</h5>
					</ModalHeader>
					<ModalBody>
						<form>
							<div className="form-group row">
								<label
									htmlFor="inputEmail3"
									className="col-sm-3 col-form-label"
								>
									Speciality
                                </label>
								<div className="col-sm-9">
									<input
										type="email"
										className="form-control"
										id="inputEmail3"
										placeholder="Enter speciality name"
										value={this.state.speciality}
										onChange={(e) => this.handleInput(e)}
									/>
									<div
										className="check-point-name-qx01 m-0 p-0"
										style={{ color: "red" }}
									>
										{this.state.specialityError}
									</div>
								</div>
							</div>
						</form>
					</ModalBody>
					<ModalFooter>
						<button
							type="button"
							className="btn btn-sm btn-secondary"
							data-dismiss="modal"
							onClick={this.modalClose}
						>
							Close
                     </button>
						<button
							type="button"
							className="btn btn-sm btn-primary"
							onClick={this.handleSubmit}
						>
							Submit
                      </button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		speciality: state.speciality.allSpeciality,
	};
};
export default connect(mapStateToProps)(UpdateSpeciality);
