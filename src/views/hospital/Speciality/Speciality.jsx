import React from "react";
import { Row, Col ,Button} from "reactstrap";
import { patients } from "variables/hospital/patients.jsx";
import Specialitylist from "../../../components/hospital/Specialitylist/Specialitylist";
import { AddSpeciality } from "./AddSpeciality";

class Speciality extends React.Component {
  state={
    modal:false
  }
  toggle = () => {
		this.setState({
			modal: !this.state.modal,
		});
	};
	modalClose = () => {
		this.setState({
			modal: false,
		});
	};

  render() {
    return (
      <div>
        <div className="content">
          <Row>
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title"> Speciality</h1>
                </div>
              </div>
              <div className="col-xl-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">All Speciality</h2>
                    <span className="btn btn-primary btn-sm-profile-btn float-right m-4" onClick={this.toggle}>
                    Add New Speciality
        	      	</span>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12">
                        <Specialitylist speciality={patients} />
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          </Row>
        </div>
        {
          this.state.modal ?
        <AddSpeciality
        modal={this.state.modal}
        toggle={this.toggle}
        modalClose={this.modalClose}
        /> : null}
      </div>

    );
  }
}

export default Speciality;
