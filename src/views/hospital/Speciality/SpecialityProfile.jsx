import React from 'react';
import {

    Row, Col,
} from 'reactstrap';

import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom';

var BASEDIR = process.env.REACT_APP_BASEDIR;
var IMGDIR = process.env.REACT_APP_IMGDIR;

class SpecialityProfile extends React.Component {
    render() {
        const allSpeciality= this.props.speciality
        return (
            <div>
                <div className="content">
                    <Row>
                        <Col xs={12} md={12}>

                            <div className="page-title">
                                <div className="float-left">
                                    <h1 className="title">Speciality Profile</h1>
                                </div>
                            </div>
                            {
                                allSpeciality ? allSpeciality.map((item, i) => (
                                    item.id === parseInt(this.props.id) ?
                                        <div className="col-xl-12">
                                            <section className="box profile-page">
                                                <div className="content-body">
                                                    <div className="col-12">
                                                        <div className="row uprofile">
                                                            <div className="uprofile-image col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                                                                <img alt="" src={IMGDIR + "/images/hospital/doctors/doctor.jpeg"} className="img-fluid" /> 
                                                            </div>
                                                            <div className="uprofile-name col-xl-10 col-lg-9 col-md-9 col-sm-8 col-12">
                                                                <h4 className="uprofile-owner">
                                                                    <p>
                                                                        {item.speciality_name}
                                                                    </p>
                                                                </h4>
                                                                <NavLink
                                                                    className="btn btn-primary btn-sm profile-btn"
                                                                    to={BASEDIR + `/dashboard/edit/${"speciality"}/${item.id}`}
                                                                    title="edit profile"
                                                                >Edit
                                                                </NavLink>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div> :null
                                )) : null
                            }
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        speciality: state.speciality.allSpeciality
    };
};
export default connect(mapStateToProps)(SpecialityProfile);
