import React, { Component } from "react";
import { connect } from "react-redux";
import {  Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";


export class AddSpeciality extends Component {
	state = {
		token: window.localStorage.getItem("token"),
		speciality: "",
		specialityError: "",
		modal: false,
	};

	handleInput = (e) => {
		this.setState({
			speciality: e.target.value,
			specialityError: "",
		});
	};

	handleSubmit = () => {
		const data = {
			speciality_name: this.state.speciality,
		};

		if (data["speciality_name"]) {
			fetch("/doctors/speciality/", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + this.state.token,
				},
				body: JSON.stringify(data),
			})
				.then((res) => res.json())
				.then((response) => {
					if (response.status) {
						this.props.dispatch({
							type: "NEW_SPECIALITY",
							Speciality: response.data,
						});
						this.setState({
							speciality: "",
							specialityError: "",
						});
					}
				});
		} else {
			this.setState({
				specialityError: "This field are required",
			});
		}
	};

	closeModel = () => {
		this.setState({
			speciality: "",
			specialityError: "",
		});
	};

	render() {
		const modal = this.props.modal;
		const toggle=this.props.toggle;
		const options = this.state.options;
		const allSpeciality = this.props.speciality;
		return (
			<div>
				<Modal isOpen={modal} toggle={toggle} className="">
					<ModalHeader toggle={toggle}>
						<h5 className="modal-title">Add New Speciality</h5>
					</ModalHeader>
					<ModalBody>
						<form>
							<div className="form-row">
								<div className="form-group col-md-12">
									<label htmlFor="inputname4">Speciality Name</label>
									<input
										type="text"
										className="form-control"
										id="inputname4"
										placeholder="Enter speciality name"
										value={this.state.speciality}
										onChange={(e) => this.handleInput(e)}
									/>
									<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
										{this.state.specialityError}
									</div>
								</div>
							</div>
						</form>
					</ModalBody>
					<ModalFooter>
						<button
							type="button"
							className="btn btn-sm btn-secondary"
							data-dismiss="modal"
							onClick={this.props.modalClose}
						>
							Close
            			</button>
						<button
							type="button"
							className="btn btn-sm btn-primary"
							onClick={this.handleSubmit}
						>
							Submit
            		</button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		speciality: state.speciality.allSpeciality,
	};
};
export default connect(mapStateToProps)(AddSpeciality);
