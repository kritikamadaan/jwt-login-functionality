import React from "react";
import { Row, Col, Label, Input } from "reactstrap";

import InputMask from "react-input-mask";

import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import moment from "moment";
import { connect } from "react-redux"
import DateTimePicker from 'react-datetime-picker';
class AddPatient extends React.Component {
	state = {
		newPatient: {
			first_name: "",
			middle_name: "",
			last_name: "",
			age: "",
			dob: "",
			email: "",
			mobile_number: "",
			gender: "",
			place_of_birth_city: "",
			place_of_birth_state: "",
			place_of_birth_country: "",
			occupation: "",
			blood: "",
			clinic: "",
			referred_by: "",
			referred_date: "",
			aadhar_no: "",
			pan_no: "",
			insurance: "",
			notes: "",
			// aadhar_image: "",
			// pan_image: "",
		},
		dobDate: moment(),
		refDate: moment(),
		firstNameError: "",
		lastNameError: "",
		attendantError: "",
		fieldError: "",
		token: window.localStorage.getItem("token"),

		newPatientAttendant: {
			attendant_name: "",
			attendant_contact: "",
			attendant_relationship: "",
		},

	}

	handleInputPatient = (name, e) => {
		this.setState({
			newPatient: { ...this.state.newPatient, [name]: e.target.value },
			firstNameError: "",
			lastNameError: "",
			clinicError: ""
		});
	}

	handleInputAttendant = (name, e) => {
		this.setState({
			newPatientAttendant: { ...this.state.newPatientAttendant, [name]: e.target.value },
			attendantError: ""
		});

	}

	handleSubmit = () => {
		let data = this.state.newPatient
		data["attendant"] = this.state.newPatientAttendant
		if (data.first_name && data.last_name && data.attendant.attendant_name && data.clinic) {

			data["insurance"] = data["insurance"] == "Yes" ? true : false

			fetch('/patients/create-profile/', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": "Bearer " + this.state.token
				},
				body: JSON.stringify(data)
			})
				.then(res => res.json())
				.then(response => {
					if (response.status) {
						this.props.dispatch({
							type: "ADD_NEW_PATIENT",
							patient: response.data
						})
						this.setState({
							newPatient: {
								first_name: "",
								middle_name: "",
								last_name: "",
								age: "",
								dob: "",
								email: "",
								mobile_number: "",
								gender: "",
								place_of_birth_city: "",
								place_of_birth_state: "",
								place_of_birth_country: "",
								occupation: "",
								blood: "",
								clinic: "",
								referred_by: "",
								referred_date: "",
								aadhar_no: "",
								// aadhar_image: "",
								pan_no: "",
								// pan_image: "",
								insurance: "",
								notes: ""
							},
							firstNameError: "",
							lastNameError: "",
							attendantError: "",
							clinicError: "",
							newPatientAttendant: {
								attendant_name: "",
								attendant_contact: "",
								attendant_relationship: "",
							},
							fieldError:""
						})
					}else{
						this.setState({
							fieldError:"All option fields and Date field are reruired"
						})
					}
				})
		} else if (!data.first_name || !data.last_name || !data.clinic) {
			
			this.setState({
				firstNameError: !data.first_name || !data.last_name ?
					"First name and Last name are required": !data.clinic ? "Clinic field are required" : "",
				lastNameError: "",
				attendantError: ""
			})
		} else if (!data.attendant.attendant_name) {
			
			this.setState({
				attendantError: "This field required",
				firstNameError: "",
				lastNameError: "",
			})
		}
	}

	handleCancle = () => {

		this.setState({
			newPatient: {
				first_name: "",
				middle_name: "",
				last_name: "",
				age: "",
				dob: "",
				email: "",
				mobile_number: "",
				gender: "",
				place_of_birth_city: "",
				place_of_birth_state: "",
				place_of_birth_country: "",
				occupation: "",
				blood: "",
				clinic: "",
				referred_by: "",
				referred_date: "",
				aadhar_no: "",
				pan_no: "",
				insurance: "",
				notes: ""
			},
			firstNameError: "",
			lastNameError: "",
			attendantError: "",
			clinicError: "",
			newPatientAttendant: ""
		})
	}

	handleChange(date, name) {

		if (name === "dob") {

			this.setState({
				dobDate: date,
				newPatient: { ...this.state.newPatient, [name]: date._i },
			});
		} else {
			this.setState({
				refDate: date,
				newPatient: { ...this.state.newPatient, [name]: date._i },
			})
		}
	}


	render() {
		const allClinics = this.props.clinic
		return (
			<div>
				<div className="content">
					<Row>
						<Col xs={12} md={12}>
							<div className="page-title">
								<div className="float-left">
									<h1 className="title">Add Patient</h1>
								</div>
							</div>

							<div className="row margin-0">
								<div className="col-12">
									<section className="box ">
										<header className="panel_header">
											<h2 className="title float-left">Basic Info</h2>
										</header>
										<div className="content-body">
											<div className="row">
												<div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
													<form>
														<div className="form-row">
															<div className="form-group col-md-12">
																<label htmlFor="inputname4">First Name</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname4"
																	placeholder="Enter first name"
																	value={this.state.newPatient.first_name}
																	onChange={(e) => this.handleInputPatient("first_name", e)}
																	required
																/>

															</div>

															<div className="form-group col-md-12">
																<label htmlFor="inputname5">Middle Name</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname5"
																	placeholder="Enter middle name"
																	value={this.state.newPatient.middle_name}
																	onChange={(e) => this.handleInputPatient("middle_name", e)}
																/>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputname6">Last Name</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname6"
																	placeholder="Enter last name"
																	value={this.state.newPatient.last_name}
																	onChange={(e) => this.handleInputPatient("last_name", e)}
																/>
																<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
																	{this.state.lastNameError}
																</div>
															</div>

														</div>
														<div className="form-row">
															<div className="form-group col-md-6">
																<label htmlFor="inputDob">Date Of Birth</label>
																<input
																	id="inputDob"
																	type="date"
																	className="form-control"
																	value={this.state.newPatient.dob}
																	onChange={(e) => this.handleInputPatient("dob", e)}
																/>
															</div>
															<div className="form-group col-md-2">
																<label htmlFor="age">Age</label>
																<input
																	type="text"
																	className="form-control"
																	id="age"
																	placeholder="age"
																	value={this.state.newPatient.age}
																	onChange={(e) => this.handleInputPatient("age", e)}
																/>
															</div>
															<div className="form-group col-md-4">
																<label htmlFor="inputGender">Gender</label>
																<select
																	id="inputGender"
																	className="form-control"
																	value={this.state.newPatient.gender}
																	onChange={(e) => this.handleInputPatient("gender", e)}
																>
																	<option value="">Select....</option>
																	<option value="male">Male</option>
																	<option value="female">Female</option>
																	<option value="transgender">Transgender</option>
																	<option value="choose not to say">Choose not to say</option>
																</select>
															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-6">
																<Label htmlFor="field-11">
																	Mobile
                                								</Label>
																<InputMask
																	id="field-11"
																	className="form-control"
																	mask="9999999999"
																	maskChar="_"
																	value={this.state.newPatient.mobile_number}
																	placeholder="Enter mobile number"
																	onChange={(e) => this.handleInputPatient("mobile_number", e)}
																/>
															</div>
															<div className="form-group col-md-6">
																<label htmlFor="exampleEmail">Email</label>
																<input
																	type="email"
																	className="form-control"
																	id="exampleEmail"
																	placeholder="Enter email"
																	value={this.state.newPatient.email}
																	onChange={(e) => this.handleInputPatient("email", e)}
																/>
															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-6">
																<label htmlFor="inputOccupation">Occupation</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputOccupation"
																	placeholder="Enter occupation"
																	value={this.state.newPatient.occupation}
																	onChange={(e) => this.handleInputPatient("occupation", e)}
																/>
															</div>
															<div className="form-group col-md-6">
																<label htmlFor="inputBlood">Blood Group</label>
																<select
																	id="inputBlood"
																	className="form-control"
																	value={this.state.newPatient.blood}
																	onChange={(e) => this.handleInputPatient("blood", e)}
																>
																	<option value="">Select...</option>
																	<option value="A+">A+</option>
																	<option value="A-">A-</option>
																	<option value="AB+">AB+</option>
																	<option value="AB-">AB-</option>
																	<option value="B+">B+</option>
																	<option value="B-">B-</option>
																	<option value="O-">O-</option>
																	<option value="O-">O-</option>
																</select>

															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-4">
																<label htmlFor="inputCity">Birth City</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputCity"
																	placeholder="Enter place of birth city"
																	value={this.state.newPatient.place_of_birth_city}
																	onChange={(e) => this.handleInputPatient("place_of_birth_city", e)}
																/>
															</div>
															<div className="form-group col-md-4">
																<label htmlFor="inputState">Birth State</label>
																<input
																	id="inputState"
																	className="form-control"
																	placeholder="Enter place of birth state"
																	value={this.state.newPatient.place_of_birth_state}
																	onChange={(e) => this.handleInputPatient("place_of_birth_state", e)}
																/>

															</div>
															<div className="form-group col-md-4">
																<label htmlFor="inputCountry">Birth Country</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputCountry"
																	placeholder="Enter place of birth country"
																	value={this.state.newPatient.place_of_birth_country}
																	onChange={(e) => this.handleInputPatient("place_of_birth_country", e)}
																/>
															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-4">
																<label htmlFor="inputCilini">Clinic</label>
																<select
																	id="inputCilini"
																	className="form-control"
																	value={this.state.newPatient.clinic}
																	onChange={(e) => this.handleInputPatient("clinic", e)}
																>
																	<option value="">Select...</option>
																	{
																		allClinics ? allClinics.map((item, i) =>
																			<option value={item.id}>{item.clinic_name}</option>
																		) : null
																	}

																</select>
															</div>
															<div className="form-group col-md-4">
																<label htmlFor="inputReffby">Referred By</label>
																<input
																	type="text"
																	id="inputReffby"
																	className="form-control"
																	placeholder="Enter name of referred by"
																	value={this.state.newPatient.referred_by}
																	onChange={(e) => this.handleInputPatient("referred_by", e)}
																/>

															</div>
															<div className="form-group col-md-4">
																<label htmlFor="inputReffDate">Reffered Date</label>
																<input
																	id="inputDob"
																	type="date"
																	className="form-control"
																	value={this.state.newPatient.referred_date}
																	onChange={(e) => this.handleInputPatient("referred_date", e)}
																/>
															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-6">
																<label htmlFor="inputIns">Insurance</label>
																<select
																	class="form-control"
																	id="inputIns"
																	value={this.state.newPatient.insurance}
																	onChange={(e) => this.handleInputPatient("insurance", e)}
																>
																	<option value="">Select...</option>
																	<option value="Yes">Yes</option>
																	<option value="No">No</option>
																</select>

															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-6">
																<label htmlFor="inputAadr">Aadhar Number</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputAadr"
																	placeholder="Enter aadhar number"
																	value={this.state.newPatient.aadhar_no}
																	onChange={(e) => this.handleInputPatient("aadhar_no", e)}
																/>
															</div>
															<div className="form-group col-md-4">
																<Label htmlFor="fileAdhar">Aadhar Image</Label>
																<Input type="file" name="file" id="fileAdhar" disabled />

															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-6">
																<label htmlFor="inputPan">PAN Number</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputPan"
																	placeholder="Enter pan number"
																	value={this.state.newPatient.pan_no}
																	onChange={(e) => this.handleInputPatient("pan_no", e)}
																/>
															</div>
															<div className="form-group col-md-4">
																<Label htmlFor="filePAn">PAN Image</Label>
																<Input type="file" name="file" id="filePAn" disabled />

															</div>
														</div>
														<div className="form-row">
															<div className="form-group col-md-8">
																<Label htmlFor="exampleText">Notes</Label>
																<Input
																	type="textarea"
																	name="text"
																	id="exampleText"
																	value={this.state.newPatient.notes}
																	onChange={(e) => this.handleInputPatient("notes", e)}
																/>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</section>
								</div>

								<div className="col-12">
									<section className="box ">
										<header className="panel_header">
											<h2 className="title float-left">Attendant Info</h2>
										</header>
										<div className="content-body">
											<div className="row">
												<div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
													<form>
														<div className="form-row">
															<div className="form-group col-md-12">
																<label htmlFor="inputAttN">Attendant Name</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputAttN"
																	placeholder="Enter attendant name"
																	value={this.state.newPatientAttendant.attendant_name}
																	onChange={(e) => this.handleInputAttendant("attendant_name", e)}
																	required
																/>
																<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
																	{this.state.attendantError}
																</div>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputAttC">Attendant Contact</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputAttC"
																	placeholder="Enter attendant contact"
																	value={this.state.newPatientAttendant.attendant_contact}
																	onChange={(e) => this.handleInputAttendant("attendant_contact", e)}
																	required
																/>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputAttR">Attendant Reletionship</label>
																<select
																	id="inputAttR"
																	className="form-control"
																	value={this.state.newPatientAttendant.attendant_relationship}
																	onChange={(e) => this.handleInputAttendant("attendant_relationship", e)}
																	required
																>
																	<option value="">Select...</option>
																	<option value="father">Father</option>
																	<option value="mother">Mother</option>
																	<option value="spouse">Spouse</option>
																	<option value="friend">Friend</option>
																	<option value="daughter">Daughter</option>
																</select>
															</div>
															<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
																{this.state.firstNameError}
															</div>
															<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
																{this.state.fieldError}
															</div>
														</div>
													</form>
													<button
														type="submit"
														className="btn btn-primary"
														style={{ float: "right" }}
														onClick={() => this.handleSubmit()}
													>
														Save
                            						</button>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</Col>
					</Row>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		clinic: state.clinic.allClinic
	};
};
export default connect(mapStateToProps)(AddPatient);

