import React from "react";
import { Row, Col, Label, Input } from "reactstrap";

import InputMask from "react-input-mask";

import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import moment from "moment";
import { connect } from "react-redux";
import Select, { NonceProvider } from "react-select";

class AddDoctor extends React.Component {
	state = {
		newDoctor: {
			first_name: "",
			middle_name: "",
			last_name: "",
			age: "",
			dob: "",
			email: "",
			mobile_number: "",
			registration_number: "",
			year_of_registration: "",
			gender: "",
			notes: ""

		},
		// newDoctor:this.props.doctor,
		editDocStatus: false,
		speciality: "",
		procedures: [],
		value: [],
		startDate: moment(),
		procedureDisable: false,
		token: window.localStorage.getItem("token"),

		options: [],
		NameError: "",
		specialityError: "",
		procedureError: ""
	}

	handleChange(date) {
		this.setState({
			startDate: date,
		});
	}
	// handle cancle
	handleCancle = () => {
		this.setState({
			newDoctor: {
				first_name: "",
				middle_name: "",
				last_name: "",
				age: "",
				dob: "",
				email: "",
				mobile_number: "",
				registration_number: "",
				year_of_registration: "",
				gender: "",
				notes: "",
			},
			speciality: "",
			procedures: [],
			value: [],
		});
	};

	//handle onchange
	onChangeProcedure = (opt) => {

		const allOptionsSelected = opt ? opt.length === this.state.options.length : false
		let valueSelect = opt ?
			opt.map((item, i) => (
				item['value']
			))
			: []
		this.setState({
			checked: allOptionsSelected ? true : false,
			value: opt,
			procedures: valueSelect,
			procedureError: ""
		});
	};

	// handle submit
	handleSubmit = () => {
		var data = this.state.newDoctor;
		data["procedures"] = this.state.procedures;
		data["speciality"] = parseInt(this.state.speciality);
		// console.log(data, ">>>>>>>>>>>>>")
		if (data.first_name && data.last_name && data.speciality && data.procedures.length && data.gender && data.dob) {
			fetch("/doctors/create-profile/", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + this.state.token,
				},
				body: JSON.stringify(data),
			})
				.then((res) => res.json())

				.then((response) => {
					
					if (response.status) {
						this.props.dispatch({
							type: "ADD_NEW_DOCTOR",
							doctor: response.data,
						});
						this.setState({
							newDoctor: {
								first_name: "",
								middle_name: "",
								last_name: "",
								age: "",
								dob: "",
								email: "",
								mobile_number: "",
								registration_number: "",
								year_of_registration: "",
								gender: "",
								notes: "",
							},
							speciality: "",
							procedures: [],
							value: [],
							NameError: ""
						});
					}
				});
		} else if (!data.first_name || !data.last_name || !data.gender || !data.dob) {
			
			this.setState({
				NameError: !data.first_name || !data.last_name ?
					"First name and Last name are required" : !data.gender ?
						"Gender are required" : !data.dob ? "Date of birth is required" : ""
			})

		} else if (!data.speciality) {
			this.setState({
				specialityError: "This field are required"
			})
		} else if (!data.procedures.length) {
			this.setState({
				procedureError: "This field are required"
			})
		}
	};

	//handle speciality
	handleSelectSpeciality = (e) => {
		let value = e.target.value;
		if (value) {
			let abc = [];
			let b = this.props.procedures
				? this.props.procedures.map((item, i) =>
					item.speciality.id == parseInt(value)
						? item.procedures.map((p, j) => abc.push({ value: p, label: p }))
						: null
				)
				: null;
			this.setState({
				speciality: e.target.value,
				procedureDisable: abc.length ? true : false,
				options: abc,
				procedureError: "",
				specialityError: ""
			});
		} else {
			this.setState({
				speciality: "",
				procedureDisable: false,
				options: [],
			});
		}
	};
	//handle input
	handleInput = (name, e) => {
		this.setState({
			newDoctor: { ...this.state.newDoctor, [name]: e.target.value },
			NameError: ""
		});
	};

	render() {
		const allSpeciality = this.props.speciality;

		return (
			<div>
				<div className="content">
					<Row>
						<Col xs={12} md={12}>
							<div className="page-title">
								<div className="float-left">
									<h1 className="title">Add Doctor</h1>
								</div>
							</div>
							<div className="row margin-0">
								<div className="col-12">
									<section className="box ">
										<header className="panel_header">
											<h2 className="title float-left">Basic Info</h2>

										</header>
										<div className="content-body">
											<div className="row">
												<div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
													<form>
														<div className="form-row">
															<div className="form-group col-md-12">
																<label htmlFor="inputname4">First Name</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname4"
																	placeholder="Enter first name"
																	value={this.state.newDoctor.first_name}
																	onChange={(e) => this.handleInput("first_name", e)}
																	required
																/>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputname5">Middle Name</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname5"
																	placeholder="Enter middle name"
																	value={this.state.newDoctor.middle_name}
																	onChange={(e) => this.handleInput("middle_name", e)}
																/>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputname6">Last Name</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname6"
																	placeholder="Enter last name"
																	value={this.state.newDoctor.last_name}
																	onChange={(e) => this.handleInput("last_name", e)}
																	required
																/>
															</div>

															<div className="form-row">
																<div className="form-group col-md-9">
																	<label htmlFor="inputDob">Date of Birth</label>
																	{/* <DatePicker
																		selected={this.state.startDate}
																		onChange={(e) => this.handleChange(e)}
																	/> */}
																	<input
																		id="inputDob"
																		type="date"
																		className="form-control"
																		value={this.state.newDoctor.dob}
																		onChange={(e) => this.handleInput("dob", e)}
																	/>
																</div>
																<div className="form-group col-md-3">
																	<label htmlFor="inputname7">Age</label>
																	<input
																		type="text"
																		className="form-control"
																		id="inputname7"
																		placeholder="age"
																		value={this.state.newDoctor.age}
																		onChange={(e) => this.handleInput("age", e)}
																		required
																	/>
																</div>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputEmail4">Email</label>
																<input
																	type="email"
																	className="form-control"
																	id="inputEmail4"
																	placeholder="Enter email"
																	value={this.state.newDoctor.email}
																	onChange={(e) => this.handleInput("email", e)}
																/>
															</div>
															<div className="form-group col-md-12">
																<Label htmlFor="field-11">Phone</Label>
																<InputMask
																	id="field-11"
																	className="form-control"
																	mask="9999999999"
																	maskChar="_"
																	placeholder="Enter mobile no."
																	value={this.state.newDoctor.mobile_number}
																	onChange={(e) => this.handleInput("mobile_number", e)}
																/>
															</div>
															<div className="form-group col-md-12">
																<Label htmlFor="exampleSelect">Gender</Label>
																<Input
																	type="select"
																	name="select"
																	id="exampleSelect"
																	value={this.state.newDoctor.gender}
																	onChange={(e) => this.handleInput("gender", e)}
																>
																	<option value="">Select....</option>
																	<option value="male">Male</option>
																	<option value="female">Female</option>
																	<option value="transgender">Transgender</option>
																	{/* <option value="choose not to say">Choose not to say</option> */}
																</Input>
															</div>
															<div className="form-group col-md-12">
																<Label htmlFor="exampleSelect1">Speciality</Label>
																<Input
																	type="select"
																	name="select"
																	id="exampleSelect1"
																	value={this.state.speciality}
																	onChange={(e) => this.handleSelectSpeciality(e)}
																	required
																>
																	{/* <option value="">Select....</option>
                                                                    <option value="male">Male</option>
                                                                    <option value="female">Female</option>
                                                                    <option value="transgender">Transgender</option>
                                                                    <option value="choose not to say">Choose not to say</option> */}
																	<option value="">Select...</option>
																	{
																		allSpeciality ? allSpeciality.map((item, i) =>
																			<option value={item.id} > {item.speciality_name} </option>
																		) : null
																	}
																</Input>
																<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
																	{this.state.specialityError}
																</div>
															</div>
															<div className="form-group col-md-12">
																<Label htmlFor="exampleSelect2">Procedures</Label>
																<Select
																	isMulti
																	onChange={this.onChangeProcedure}
																	options={this.state.options}
																	value={this.state.value}
																	isDisabled={!this.state.procedureDisable}
																/>
																<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
																	{this.state.procedureError}
																</div>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputname8">Registration No.</label>
																_<input
																	type="text"
																	className="form-control"
																	id="inputname8"
																	placeholder="Enter registration no."
																	value={this.state.newDoctor.registration_number}
																	onChange={(e) => this.handleInput("registration_number", e)}
																	required
																/>
															</div>
															<div className="form-group col-md-12">
																<label htmlFor="inputname9">Registration Years</label>
																<input
																	type="text"
																	className="form-control"
																	id="inputname9"
																	placeholder="Enter year of registraion"
																	value={this.state.newDoctor.year_of_registration}
																	onChange={(e) => this.handleInput("year_of_registration", e)}
																	required
																/>
															</div>
															<div className="form-group col-md-12">
																<Label htmlFor="exampleText">Notes</Label>
																<Input
																	type="textarea"
																	name="text"
																	id="exampleText"
																	value={this.state.newDoctor.notes}
																	onChange={(e) => this.handleInput("notes", e)}
																/>
															</div>
															{/* <div className="form-group col-md-12">
                                                                <Label htmlFor="exampleFile">Profile Image</Label>
                                                                <div className="profileimg-input"><img alt="" src={IMGDIR + "/images/hospital/doctors/doctor-1.jpg"} className="img-fluid" style={{ "width": "120px" }} /></div>
                                                                <Input type="file" name="file" id="exampleFile" />
                                                            </div> */}
															<div className="check-point-name-qx01 m-0 p-0" style={{ color: "red" }}>
																{this.state.NameError}
															</div>
														</div>

													</form>
													<button
														type="submit"
														className="btn btn-primary"
														style={{ float: "right" }}
														onClick={() => this.handleSubmit()}
													>
														Save
                            						</button>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</Col>
					</Row>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		speciality: state.speciality.allSpeciality,
		procedures: state.procedure.allProcedures,
	};
};

export default connect(mapStateToProps)(AddDoctor);
