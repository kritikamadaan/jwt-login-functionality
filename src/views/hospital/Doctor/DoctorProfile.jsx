import React from 'react';
import {

    Row, Col,
} from 'reactstrap';

import {

} from 'components';
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom';

var BASEDIR = process.env.REACT_APP_BASEDIR;
var IMGDIR = process.env.REACT_APP_IMGDIR;

class DoctorProfile extends React.Component {
    // state = {
    //     token: window.localStorage.getItem("token"),
    //     specility:""
    // }
    // componentDidMount() {
    //     const token = this.state.token
    //     fetch(`/doctors/details/${this.props.id}/`, {
    //         method: "GET",
    //         headers: {
    //             "Content-Type": "application/json",
    //             "Authorization": "Bearer " + token
    //         },
    //     })
    //         .then(res => res.json())
    //         .then(res => { 
    //             console.log(res)
    //             this.setState({
    //                 doc:res,
    //                 speciality:res.speciality.speciality_name
    //             })
    //         })
    // }

    render() {
        const allDoctor = this.props.doctor
        console.log('doctor ',allDoctor)
        return (
            <div>
                <div className="content">
                    <Row>
                        <Col xs={12} md={12}>

                            <div className="page-title">
                                <div className="float-left">
                                    <h1 className="title">Doctor Profile</h1>
                                </div>
                            </div>
                            {
                                allDoctor ? allDoctor.map((item, i) => (
                                    item.id === parseInt(this.props.id) ?
                                        <div className="col-xl-12">
                                            <section className="box profile-page">
                                                <div className="content-body">
                                                    <div className="col-12">
                                                        <div className="row uprofile">
                                                            <div className="uprofile-image col-xl-2 col-lg-3 col-md-3 col-sm-4 col-12">
                                                                <img alt="" src={IMGDIR + "/images/hospital/doctors/doctor.jpeg"} className="img-fluid" />
                                                            </div>
                                                            <div className="uprofile-name col-xl-10 col-lg-9 col-md-9 col-sm-8 col-12">
                                                                <h3 className="uprofile-owner">
                                                                    <p>Dr. {
                                                                        item.middle_name ?
                                                                            item.first_name.toUpperCase() + " " + item.middle_name.toUpperCase() + " " + item.last_name.toUpperCase() :
                                                                            item.first_name.toUpperCase() + " " + item.last_name.toUpperCase()
                                                                    }
                                                                    </p>
                                                                </h3>
                                                                <NavLink
                                                                    className="btn btn-primary btn-sm profile-btn"
                                                                    to={BASEDIR + `/dashboard/edit/${"doctor"}/${item.id}`}
                                                                    title="edit profile"
                                                                >Edit
                                                                </NavLink>
                                                                {/* <button className="btn btn-primary btn-sm profile-btn">Follow</button> */}
                                                                <div className="clearfix"></div>
                                                                <p className="uprofile-title">{item.speciality.speciality_name}</p>
                                                                <div className="clearfix"></div>
                                                                <p>{item.notes}</p>
                                                                <p className="uprofile-list">
                                                                    {/* <span> {item.age} years old</span> */}
                                                                    <span><i className='i-phone'></i> {item.mobile_number}</span>
                                                                    <span><i className='i-envelope icon-primary icon-xs'></i> {item.email}</span>

                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-12">

                                                        {/* <h4>Biography:</h4>
                                                        <p>In the United States, the Ed.D. tends to be granted by the school of education of universities and is a terminal degree in education. Majors within the Ed.D. may include: counseling, curriculum and instruction/curriculum and teaching, educational administration, education policy, educational psychology, educational technology, higher education, human resource development, language/linguistics or leadership. </p>
                                                        <p>In the United States, the Ed.D. tends to be granted by the school of education of universities and is a terminal degree in education. Majors within the Ed.D. may include: counseling, curriculum and instruction/curriculum and teaching, educational administration, education policy, educational psychology, educational technology, higher education, human resource development, language/linguistics or leadership. </p> */}
                                                        <div className="clearfix"></div>
                                                        <hr />
                                                        <h4>Personal Info:</h4>
                                                        <ul>
                                                            <ul>
                                                                <li>Age: {item.age} years old</li>
                                                                <li>Gender: {item.gender}</li>
                                                                <li>DOB: {item.dob}</li>
                                                            </ul>
                                                        </ul>
                                                        <div className="clearfix"></div>
                                                        <hr />
                                                        <h4>Speciality:</h4>
                                                        <ul>
                                                            <h4>{item.speciality.speciality_name}</h4>
                                                            <ul>
                                                                {
                                                                    item.procedures.map((p, j) => (
                                                                        <li>{p}</li>
                                                                    ))
                                                                }
                                                            </ul>
                                                        </ul>

                                                        <div className="clearfix"></div>
                                                        <hr />
                                                        <h4>Registration</h4>
                                                        <ul>
                                                            <ul>
                                                                <li>Number: {item.registration_number}</li>
                                                                <li>Years: {item.year_of_registration}</li>
                                                            </ul>
                                                        </ul>
                                                        <div className="clearfix"></div>

                                                    </div>
                                                </div>
                                            </section>
                                        </div> :null
                                )) : null
                            }
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        doctor: state.doctor.allDoctor
    };
};
export default connect(mapStateToProps)(DoctorProfile);
// export default DoctorProfile;
