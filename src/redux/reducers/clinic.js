const initialState = {
    allClinic:[],
    newClinic:[]
  };

  const clinicReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_ALL_CLINIC":
            let allClinic = action.clinic
            return {
                ...state,
                allClinic
            };
        case "ADD_NEW_CLINIC":
            let newClinic = [...state.allClinic]
            newClinic.push(action.clinic)
            return {
                ...state,
                allClinic:newClinic
            };
        default:
            return state;
    }
  };
  
  export default clinicReducer;
  