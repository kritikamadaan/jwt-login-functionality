const initialState = {
	allAppointments: [],
	newAppointments: []
};
const appointmentReducer = (state = initialState, action) => {
	switch (action.type) {
		case "ALL_APOINTMENTS_LIST":
            let allAppointments = action.appointment
            return {
                ...state,
                allAppointments
            };
        case "ADD_NEW_APPOINTMENT":
            let newAppointments = [...state.allAppointments]
            newAppointments.push(action.appointment)
            return {
                ...state,
                allAppointments:newAppointments
            };
		default:
			return state;
		}
};

export default appointmentReducer;
