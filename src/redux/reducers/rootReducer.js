import { combineReducers } from "redux";
import authReducer from "./auth";
import specialityReducer from "./speciality"
import clinicReducer from "./clinic"
import doctorReducer from "./doctor"
import patientReducer from "./patient"
import appointmentReducer from "./appointment"
import proceduresReducer from "./procedure"

const rootReducer = combineReducers({
    auth: authReducer,
    speciality: specialityReducer,
    clinic: clinicReducer,
    doctor: doctorReducer,
    patient:patientReducer,
    appointment:appointmentReducer,
    procedure:proceduresReducer
});

export default rootReducer;
