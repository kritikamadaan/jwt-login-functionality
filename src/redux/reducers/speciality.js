const initialState = {
    allSpeciality:[],
    newSpeciality:[]
  };

  const specialityReducer = (state = initialState, action) => {
    switch (action.type) {
        case "All_SPECIALITY":
            let allSpeciality = action.Speciality
        return {
            ...state,
            allSpeciality
        };
        case "NEW_SPECIALITY":
            let newSpeciality = [...state.allSpeciality]
            newSpeciality.push(action.Speciality)
        return {
            ...state,
            allSpeciality:newSpeciality
        };
        default:
            return state;
    }
  };
  
  export default specialityReducer;
  