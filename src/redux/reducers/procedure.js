const initialState = {
    allProcedures:[],
    newProcedures:[]
  };

  const proceduresReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_ALL_PROCEDURES":
            let allProcedures = action.procedure
            return {
                ...state,
                allProcedures
            };
        case "ADD_NEW_PROCEDURES":
            let newProcedures= [...state.allProcedures]
            newProcedures.push(action.procedure)
            return {
                ...state,
                allProcedures:newProcedures
            };
        default:
            return state;
    }
  };
  
  export default proceduresReducer;
  