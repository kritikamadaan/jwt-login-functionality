const initialState = {
    loading: true,
    token: "",
    email: "",
    name : "",
    userId: "",
    isSuperUser: false,
    has_write_access: false,

    Clinics : 0,
    Patients : 0,
    Doctors : 0,
    Appointments : 0,
    Speciality : 0
  };
  const authReducer = (state = initialState, action) => {
    switch (action.type) {
      case "START_LOADING":
      return {
        ...state,
        loading: true
      };

      case "STOP_LOADING":
        return {
          ...state,
          loading: false
      };

      case "SUPER_USER_INFO":
        let email = ""
        let name = ""
        let userId = ""
        email = action.userDetails.email;
        name = action.userDetails.name
        userId = action.userDetails.id
        
        return {
          ...state,
          email,
          name,
          userId,
          isSuperUser:action.userDetails.is_superuser,
          token:action.token
        };
      case "TOTAL_HOSPITAL_DATA":
        // let Clinics = 0
        // let Patients = 0
        // let Doctors = 0
        // let Appointments = 0
        // let Speciality = 0
        return {
            ...state,
            Clinics : action.total.clinics,
            Patients : action.total.patients,
            Doctors : action.total.doctors,
            Appointments : action.total.appointments,
            Speciality : action.total.speciality
          };
      case "LOGOUT":
        return {
          ...initialState,
          loading: false
        };
      default:
        return state;
    }
  };
  
  export default authReducer;
  