const initialState = {
	allDoctor: [],
	newDoctor: [],
	editDoctor: {},
	editDocStatus: false
};
const doctorReducer = (state = initialState, action) => {
	switch (action.type) {
		case "ALL_DOCTOR_LIST":
			let allDoctor = action.doctor
			return {
				...state,
				allDoctor
			}
		case "ADD_NEW_DOCTOR":
			let newDoctor = [...state.allDoctor]
			newDoctor.push(action.doctor)
			return {
				...state,
				allDoctor: newDoctor
			}
		case "EDIT_DOCTOR_PROFILE":
			let editDoctor = action.editDoctor
			let editDocStatus = action.edit
			return {
				...state,
				editDoctor,
				editDocStatus
			}
		default:
			return state;
	}
};

export default doctorReducer;
