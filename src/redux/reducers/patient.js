import { act } from "react-dom/test-utils";

const initialState = {
	allPatients: [],
	newPatients: []
};
const patientReducer = (state = initialState, action) => {
	switch (action.type) {

		case "ALL_PATIENT_DATA":
			let allPatients = action.patient
			return{
				...state,
				allPatients
			}
		
		case "ADD_NEW_PATIENT":
			let newPatients = [...state.allPatients]
			newPatients.push(action.patient)
			return{
				...state,
				allPatients: newPatients
			}
		default:
			return state;
	}
};

export default patientReducer;
