import React from 'react';
//import { DropdownMenu, DropdownItem, } from 'reactstrap';
// used for making the prop types of this component
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux'
import EditingProcedures from '../../../views/Procedures/EditingProcedures';

var BASEDIR = process.env.REACT_APP_BASEDIR;


class Procedurelist extends React.Component {
    state = {
        modal: false,
        procedureId: ""
    }
    modalOpenHandler = (id) => {
        this.setState({
            modal: !this.state.modal,
            procedureId: id
        })
    }
    closeModal = () => {
        this.setState({
            modal: false
        })
    }
    render() {

        const allProcedure = this.props.procedure
        
        return (
            <div className="row">

                {
                    allProcedure ? allProcedure.map((item, i) => (
                        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3" key={i}>
                            <div className="team-member">
                                <div className="team-img">
                                    {/* <img className="img-fluid" src={this.props.patients[i].avatar} alt="" /> */}
                                </div>
                                {/* <NavLink to={BASEDIR + `/dashboard/edit/${"procedure"}/${item.id}`} title="edit procedure" style={{ float: "right" }}><i className="fa fa-edit" /></NavLink> */}
                                <NavLink
                                    className="nav-link"
                                    data-toggle="modal"
                                    data-target="#editProcedureModalCenter"
                                    style={{ cursor: "pointer" }}
                                    onClick={() => { this.modalOpenHandler(item.id) }}
                                ><i className="fa fa-edit"/></NavLink>

                                <div className="team-info">
                                    <h4>
                                        <NavLink to={BASEDIR + `/dashboard/${"procedure"}/profile/${item.id}`}>
                                            {
                                                item.speciality.speciality_name
                                            }
                                        </NavLink>
                                    </h4>
                                    {/* <span>{this.props.patients[i].position}</span> /  */}
                                    {/* <span>{item.speciality.id}</span> */}
                                    <ul >
                                        <ul>
                                            {
                                                item.procedures.map((p, j) => (
                                                    <li>{p} </li>
                                                ))
                                            }
                                        </ul>
                                    </ul>
                                </div>

                            </div>

                        </div>
                    )) : null
                }
                {/* {patientsList} */}
                {
                    this.state.modal
                        ? <EditingProcedures
                            modal={this.state.modal}
                            openHandler={this.openModalHandler}
                            closeModal={this.closeModal}
                            id={this.state.procedureId}
                        />
                        : null
                }

            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        procedure: state.procedure.allProcedures
    };
};
export default connect(mapStateToProps)(Procedurelist);

// export default Patientslist;
