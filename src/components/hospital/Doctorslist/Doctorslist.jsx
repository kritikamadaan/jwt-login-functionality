import React from 'react';
//import { DropdownMenu, DropdownItem, } from 'reactstrap';
// used for making the prop types of this component
import PropTypes from 'prop-types';
import { NavLink, Link } from 'react-router-dom';
import { connect } from "react-redux"

var BASEDIR = process.env.REACT_APP_BASEDIR;
var IMGDIR = process.env.REACT_APP_IMGDIR;

class Doctorslist extends React.Component {
    render() {
        const allDoctors = this.props.doctor
        // console.log(allDoctors)
        // var doctorsList = [];
        // for (var i = 0; i < this.props.doctors.length; i++) {
        //     doctorsList.push(

        //         <div className="col-md-6 col-lg-4" key={i}>
        //             <div className="team-member ">
        //                 <div className="team-img">
        //                     {/* <img className="img-fluid" src={this.props.doctors[i].avatar} alt="" /> */}
        //                 </div>
        //                 <div className="team-info">
        //                     <h3><NavLink to={BASEDIR + "/dashboard/doctor-profile"}>{this.props.doctors[i].name}</NavLink></h3>
        //                     <span>{this.props.doctors[i].position}</span>
        //                     <p>{this.props.doctors[i].msg}</p>
        //                 </div>
        //             </div>
        //         </div>
        //     );
        // }
        
        return (
            <div className="row">
                {
                    allDoctors ? allDoctors.map((item, i) => (
                        <div className="col-md-3" key={i}>
                            <div className="team-member ">
                                <div className="team-img">
                                    <img className="img-fluid" src={IMGDIR + "/images/hospital/doctors/doctor.jpeg"} alt="" />
                                </div>
                                <NavLink to={BASEDIR + `/dashboard/edit/${"doctor"}/${item.id}`} title="edit profile" style={{ float: "right" }}><i className="fa fa-edit"/></NavLink>
                                <div className="team-info">
                                    
                                    <h3>

                                        <NavLink to={BASEDIR + `/dashboard/${"doctor"}/profile/${item.id}`} title="open profile">
                                            {
                                                item.middle_name ?
                                                    "Dr. " + item.first_name + " " + item.middle_name + " " + item.last_name :
                                                    "Dr. " + item.first_name + " " + item.last_name
                                            }
                                        </NavLink>
                                    </h3>
                                    <span>{item.speciality.speciality_name}</span>
                                    <p>{item.notes}</p>

                                </div>

                            </div>
                        </div>
                    )) : null
                }
                {/* {doctorsList} */}
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        doctor: state.doctor.allDoctor
    };
};
export default connect(mapStateToProps)(Doctorslist);


// export default Doctorslist;
