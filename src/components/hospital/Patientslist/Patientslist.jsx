import React from 'react';
//import { DropdownMenu, DropdownItem, } from 'reactstrap';
// used for making the prop types of this component
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux'

var BASEDIR = process.env.REACT_APP_BASEDIR;


class Patientslist extends React.Component {
    render() {        
        const allPatient = this.props.patient
        return (
            <div className="row">
                {
                    allPatient ? allPatient.map((item, i) => (
                        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3" key={i}>
                            <div className="team-member">
                                <div className="team-img">
                                    {/* <img className="img-fluid" src={this.props.patients[i].avatar} alt="" /> */}
                                </div>
                                <NavLink to={BASEDIR + `/dashboard/edit/${"patient"}/${item.id}`} title="edit profile" style={{ float: "right" }}><i className="fa fa-edit"/></NavLink>
                                <div className="team-info">
                                    <h4>
                                        <NavLink to={BASEDIR + `/dashboard/${"patient"}/profile/${item.id}`}>
                                            {
                                                item.middle_name ?
                                                    item.first_name + " " + item.middle_name + " " + item.last_name :
                                                    item.first_name + " " + item.last_name
                                            }
                                        </NavLink>
                                    </h4>
                                    {/* <span>{this.props.patients[i].position}</span> /  */}
                                    <span>{item.age} years old</span>
                                    {/* <ul className="social-icons list-inline list-unstyled">
                                        <li className="list-inline-item"><a href="#!"><i className="i-envelope icon-primary icon-xs"></i></a></li>
                                    </ul> */}
                                </div>
                            </div>
                        </div>
                    )) : null
                }
                {/* {patientsList} */}
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        patient: state.patient.allPatients
    };
};
export default connect(mapStateToProps)(Patientslist);

// export default Patientslist;
