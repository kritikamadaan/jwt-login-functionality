import React from 'react';
//import { DropdownMenu, DropdownItem, } from 'reactstrap';
// used for making the prop types of this component
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import {connect} from "react-redux";


var BASEDIR = process.env.REACT_APP_BASEDIR;
var IMGDIR = process.env.REACT_APP_IMGDIR;

class Appointmentlist extends React.Component{
    render(){
        
        const appointments = this.props.appointments 

        return (
            <div className="row">
            {
                appointments ? appointments.map((item, i) => (
                    <div className="col-md-3" key={i}>
                        <div className="team-member ">
                            <div className="team-img">
                                <img className="img-fluid" src={IMGDIR + "/images/hospital/doctors/doctor.jpeg"} alt="" />
                            </div>
                            <NavLink to={BASEDIR + `/dashboard/edit/${"appointment"}/${item.id}`} title="edit profile" style={{ float: "right" }}><i className="fa fa-edit"/></NavLink>
                            <div className="team-info">
                                
                                <h3>

                                    <NavLink to={BASEDIR + `/dashboard/${"appointment"}/profile/${item.id}`} title="open profile">
                                        {
                                            item.doctor.first_name ?
                                                "Dr. " + item.doctor.first_name + " " + item.doctor.middle_name + " " + item.doctor.last_name :
                                                "Dr. " + item.doctor.first_name + " " + item.doctor.last_name
                                        }
                                    </NavLink>
                                </h3>
                                <span>{item.doctor.age}</span>
                                <p>{item.doctor.mobile_number}</p>

                            </div>

                        </div>
                    </div>
                )) : null
            }
           
        </div>             
        );
    }
}


const mapStateToProps = (state) => {
    
    return{
        appointments: state.appointment.allAppointments
    }
} 
export default connect(mapStateToProps)(Appointmentlist)
