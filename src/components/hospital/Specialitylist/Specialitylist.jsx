import React from 'react';
//import { DropdownMenu, DropdownItem, } from 'reactstrap';
// used for making the prop types of this component
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import {connect} from "react-redux";
import { EditSpeciality } from '../../../views/hospital/Speciality/EditSpeciality';


var BASEDIR = process.env.REACT_APP_BASEDIR;


class Specialitylist extends React.Component{
    state = {
        modal: false,
        specialityId: ""
    }
    modalOpenHandler = (id) => {
        this.setState({
            modal: !this.state.modal,
            specialityId: id
        })
    }
    closeModal = () => {
        this.setState({
            modal: false
        })
    }
   
    render(){
      const speciality = this.props.speciality
        return (
            <div className="row">

            {
                speciality ? speciality.map((item, i) => (
                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3" key={i}>
                        <div className="team-member">
                            <div className="team-img">
                                {/* <img className="img-fluid" src={this.props.patients[i].avatar} alt="" /> */}
                            </div>
                              {/* <NavLink to={BASEDIR + `/dashboard/edit/${"speciality"}/${speciality[i].id}`} title="edit profile" style={{ float: "right" }}>  */}
                            <span
                                className="nav-link"
                                data-toggle="modal"
                                data-target="#editProcedureModalCenter"
                                style={{ cursor: "pointer" }}
                                onClick={() => { this.modalOpenHandler(item.id) }}
                            > <i className="fa fa-edit"/>
                            </span>
                            {/* </NavLink> */}
                            <div className="team-info">
                            
                                <h4 >
                                <NavLink to={BASEDIR + `/dashboard/${"speciality"}/profile/${speciality[i].id}`} title="open profile">
                                    {speciality[i].speciality_name}
                                    </NavLink>
                                    </h4>
                                <span>{speciality[i].speciality_location}</span>               
                                  
                                   
                             
                                {/* <span>{this.props.patients[i].position}</span> /  */}
                                {/* <span>{item.speciality.id}</span> */}
                               
                            </div>

                        </div>

                    </div>
                )) : null
            }
            {/* {patientsList} */}
            {
            this.state.modal
                ? <EditSpeciality
                    modal={this.state.modal}
                    openHandler={this.openModalHandler}          
                    closeModal={this.closeModal}
                    id={this.state.specialityId}
                />
            : null
        }

        </div>          
        );
    }
}
Specialitylist.propTypes = {
    speciality: PropTypes.arrayOf(PropTypes.object)
}

const mapStateToProps = (state) => {
    
    return{
        speciality: state.speciality.allSpeciality
    }
} 
export default connect(mapStateToProps)(Specialitylist)






// const speciality = this.props.speciality
    
// var specialityList = [];
// for (var i = 0; i < speciality.length; i++) {

//     specialityList.push(
//         <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3" key={i}>
//             <div className="team-member">
//                 <div className="team-img">
//                     {/* <img className="img-fluid" src={this.props.patients[i].avatar} alt="" /> */}

//                 </div>
//                 {/* <NavLink to={BASEDIR + `/dashboard/edit/${"speciality"}/${speciality[i].id}`} title="edit profile" style={{ float: "right" }}> */}
//                     <span  
//                     onClick={() => { this.modalOpenHandler(speciality[i].id) }}>
//                      <i className="fa fa-edit"/>
//                     </span>
//                     {/* </NavLink> */}
//                 <div className="team-info">

//                     <h4 ><NavLink to={BASEDIR + `/dashboard/${"speciality"}/profile/${speciality[i].id}`} title="open profile">{speciality[i].speciality_name}</NavLink></h4>
//                     <span>{speciality[i].speciality_location}</span>
//                     <ul className="social-icons list-inline list-unstyled">
//                         <li className="list-inline-item"><a href="#!"><i className="i-envelope icon-primary icon-xs"></i></a></li>
//                     </ul>
//                 </div>
//             </div>
//         </div>
//     );
// }


       
// {
//     this.state.modal
//         ? <EditSpeciality
//             modal={this.state.modal}
//             openHandler={this.openModalHandler}
//             closeModal={this.closeModal}
//             id={this.state.specialityId}
//         />
//         : null
// }