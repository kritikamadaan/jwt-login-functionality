import React from 'react';
//import { DropdownMenu, DropdownItem, } from 'reactstrap';
// used for making the prop types of this component
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from "react-redux";


var BASEDIR = process.env.REACT_APP_BASEDIR;


class Cliniclist extends React.Component {
    render() {
        const allAlinic = this.props.clinics
    
        var clinicsList = []    
        const list= allAlinic? allAlinic.map((item,i)=>(
            clinicsList.push(
                <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3" key={i}>
                    <div className="team-member">
                        <div className="team-img">
                            {/* <img className="img-fluid" src={this.props.patients[i].avatar} alt="" /> */}

                        </div>
                        <NavLink to={BASEDIR + `/dashboard/edit/${"clinic"}/${item.id}`} title="edit profile" style={{ float: "right" }}><i className="fa fa-edit"/></NavLink>
                        <div className="team-info">

                            <h4><NavLink to={BASEDIR + `/dashboard/${"clinic"}/profile/${item.id}`} title="open profile">{item.clinic_name}</NavLink></h4>
                            <span>{item.clinic_location}</span>
                            {/* <ul className="social-icons list-inline list-unstyled">
                                <li className="list-inline-item"><a href="#!"><i className="i-envelope icon-primary icon-xs"></i></a></li>
                            </ul> */}
                        </div>
                    </div>
                </div>
            )
        )):null
        return (
            <div className="row">
                {clinicsList}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    
    return {
        clinics: state.clinic.allClinic
    }
}
export default connect(mapStateToProps)(Cliniclist)
