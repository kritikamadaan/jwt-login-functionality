import React, { Component } from "react";

import { createBrowserHistory } from 'history';
import {
    Router,
    Route,
    Switch
} from 'react-router-dom';
import { connect } from "react-redux"
import 'bootstrap/dist/css/bootstrap.css';
import '@fortawesome/fontawesome-free/css/all.css'

/*import 'font-awesome/css/font-awesome.min.css';*/
import 'assets/scss/zest-admin.css';
import 'assets/fonts/simple-line-icons.css';

import indexRoutes from 'routes/index.jsx';
import LoginPage from 'auth/LoginPage.jsx';
import Register from "auth/pages/register";
import ForgotPassword from "auth/pages/forgotPassword"
import ProtectedRoute from 'ProtectedRoute';

const hist = createBrowserHistory();

var BASEDIR = process.env.REACT_APP_BASEDIR;


class App extends Component {

    componentDidMount() {
        const token = window.localStorage.getItem("token");
        this.props.dispatch({
            type: "START_LOADING"
        });
        if (token && !this.props.email) {
            this.loadData();
        } else {
            this.props.dispatch({
                type: "STOP_LOADING"
            });
        }
    }
    //   componentDidUpdate(prevProps) {
    //     const token = window.localStorage.getItem("token");
    //     if (!token) return;
    //     if (!prevProps.token && token) return;
    //     this.loadData();
    //   }

    loadData = () => {
        const token = window.localStorage.getItem("token");
        this.props.dispatch({
            type: "START_LOADING"
        });
        fetch(`/users/list/`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            },
        })
            .then(res => res.json())
            .then(res => {
                if (Object.keys(res).includes('email')) {
                    window.localStorage.setItem("token", token);
                    // localStorage.removeItem('token');
                    let userDetails = res.email
                    this.props.dispatch({
                        type: "SUPER_USER_INFO",
                        userDetails,
                        token
                    });
                    this.props.dispatch({
                        type: "TOTAL_HOSPITAL_DATA",
                        total: res.total
                    });
                    this.props.dispatch({
                        type: "STOP_LOADING"
                    });

                    fetch(`/comman/data/`, {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                            "Authorization": "Bearer " + token
                        },
                    })
                        .then(res => res.json())
                        .then(response => {

                            this.props.dispatch({
                                type: "ALL_DOCTOR_LIST",
                                doctor: response.doctor
                            })

                            this.props.dispatch({
                                type: "All_SPECIALITY",
                                Speciality: response.speciality
                            });

                            this.props.dispatch({
                                type: "ADD_ALL_CLINIC",
                                clinic: response.clinic
                            })

                            this.props.dispatch({
                                type: "ALL_PATIENT_DATA",
                                patient: response.patient
                            })
                            this.props.dispatch({
                                type: "ALL_APOINTMENTS_LIST",
                                appointment: response.appointments
                            })
                            this.props.dispatch({
                                type: "ADD_ALL_PROCEDURES",
                                procedure: response.procedures
                            })
                        })
                } else {
                    let userDetails = {
                        "name": "",
                        "email": "",
                        "id": "",
                        "is_superuser": false
                    }
                    this.props.dispatch({
                        type: "SUPER_USER_INFO",
                        userDetails,
                        token: ''
                    });
                    this.props.dispatch({
                        type: "STOP_LOADING"
                    });
                }

            })
    }

    render() {

        var routes = [
            <Route exact path={BASEDIR + "/login"} component={LoginPage} />,
            <Route exact path={BASEDIR + "/register"} component={Register} />,
            <Route exact path={BASEDIR + "/forgot/password"} component={ForgotPassword} />,
        ]
        for (var i = 0; i < indexRoutes.length; i++) {
            routes.push(
                <Route
                    path={indexRoutes[i].path}
                    key={i}
                    component={indexRoutes[i].component}
                />
            )
        }
        return (
            // this.props.loading ? (
            //     <h2>Loading...</h2>
            // ) :
            //     (
            // <Router history={hist} basename={process.env.REACT_APP_BASEDIR}>
            <Switch>
                {/* {
                    indexRoutes.map((prop, key) => {
                        //console.log(prop.path + prop.key);
                        return (
                            <Route
                                path={prop.path}
                                key={key}
                                component={prop.component}
                            />
                        );
                    })
                } */}
                {routes}
            </Switch>
            // </Router>
        );
    }
}

// const mapStateToProps = state => {
//   return {
// 	email: state.auth.email,
// 	loading: state.auth.loading,
// 	token: state.auth.token
//   };
// };
// export default connect(mapStateToProps)(App);

export default connect()(App)
