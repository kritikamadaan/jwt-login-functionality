
import Hospital from 'views/hospital/Dashboard/Hospital.jsx';
import Doctor from 'views/hospital/Doctor/Doctor.jsx';
import AddDoctor from 'views/hospital/Doctor/AddDoctor.jsx';
import EditDoctor from 'views/hospital/Doctor/EditDoctor.jsx';
import DoctorProfile from 'views/hospital/Doctor/DoctorProfile.jsx';

import Speciality from 'views/hospital/Speciality/Speciality.jsx';
import AddSpeciality from 'views/hospital/Speciality/AddSpeciality.jsx';


// import Procedures from 'views/hospital/Procedures/Procedures.jsx';
// import AddProcedures from 'views/hospital/Procedures/AddProcedures.jsx';


import Patient from 'views/hospital/Patient/Patient.jsx';
import AddPatient from 'views/hospital/Patient/AddPatient.jsx';
import EditPatient from 'views/hospital/Patient/EditPatient.jsx';
import PatientProfile from 'views/hospital/Patient/PatientProfile.jsx';

import DoctorCalendar from 'views/hospital/Appointment/DoctorCalendar.jsx';
import BookSlot from 'views/hospital/Appointment/BookSlot.jsx';
import Appointmentlist from '../components/hospital/Appointmentlist/Appointmentlist';

import Staff from 'views/hospital/Staff/Staff.jsx';
import AddStaff from 'views/hospital/Staff/AddStaff.jsx';
import EditStaff from 'views/hospital/Staff/EditStaff.jsx';
import StaffProfile from 'views/hospital/Staff/StaffProfile.jsx';

import HospitalEvents from 'views/hospital/Events/HospitalEvents.jsx';
import AddEvent from 'views/hospital/Events/AddEvent.jsx';

import HospitalPayments from 'views/hospital/Billing/HospitalPayments.jsx';
import HospitalInvoice from 'views/hospital/Billing/HospitalInvoice.jsx';
import HospitalAddPayment from 'views/hospital/Billing/HospitalAddPayment.jsx';
import HospitalLocations from 'views/hospital/Locations/HospitalLocations.jsx';

import HospitalMailinbox from 'views/hospital/Mail/Inbox.jsx';
import HospitalMailcompose from 'views/hospital/Mail/Compose.jsx';
import HospitalMailview from 'views/hospital/Mail/View.jsx';

import HospitalReportsHospital from 'views/hospital/Reports/ReportHospital.jsx'; 
import HospitalReportsPatient from 'views/hospital/Reports/ReportPatients.jsx'; 
import HospitalReportsSales from 'views/hospital/Reports/ReportSales.jsx'; 
import HospitalReportsStats from 'views/hospital/Reports/ReportStats.jsx'; 

import AddProcedures from '../views/Procedures/AddProcedures';
import Procedures from '../views/Procedures/Procedures';
import EditProcedures from '../views/Procedures/EditingProcedures';
import Specialitylist from '../components/hospital/Specialitylist/Specialitylist';
import Clinic from '../views/hospital/Clinic/Clinic';
import AddClinic from '../views/hospital/Clinic/AddClinic';
import AddAppointment from '../views/Appointments/AddAppointment';
import Appointment from '../views/Appointments/Appointment';
import EditAppointment from '../views/Appointments/Editappointment';
import { UpdateAppointment } from '../views/Appointments/UpdateAppointment';
import { UpdateClinic } from '../views/hospital/Clinic/UpdateClinic';
import UpdatePatient from '../views/hospital/Patient/UpdatePatient';
import UpdateDoctor from '../views/hospital/Doctor/UpdateDoctor';
import { UpdateSpeciality } from '../views/hospital/Speciality/UpdateSpeciality';
import UpdateProcedure from '../views/Procedures/UpdateProcedures';



var BASEDIR = process.env.REACT_APP_BASEDIR;



var dashRoutes = [ 

    { path:BASEDIR+"/dashboard", name: "Dashboard", icon: "speedometer", badge: "", component: Hospital },


    { 
        path: "#", name: "Doctors", icon: "user", type: "dropdown", parentid:"doctors",
            child: [
                { path: BASEDIR+"/dashboard/doctors", name: "Doctors"},
                { path: BASEDIR+"/dashboard/add-doctor", name: "Add Doctor"},
                // { path: BASEDIR+"/dashboard/update-doctor", name: "Update Doctor"},
                // { path: BASEDIR+"/dashboard/edit-doctor", name: "Edit Doctor"},
                // { path: BASEDIR+"/dashboard/doctor-profile", name: "Doctor Profile"},
            ]
    },
        { path: BASEDIR+"/dashboard/doctors", component: Doctor, type: "child"},
        { path: BASEDIR+"/dashboard/add-doctor", component:AddDoctor, type: "child"},
        // { path: BASEDIR+"/dashboard/update-doctor", component: UpdateDoctor, type: "child"},
        // { path: BASEDIR+"/dashboard/edit-doctor", component: EditDoctor, type: "child"},
        // { path: BASEDIR+"/dashboard/doctor-profile", component: DoctorProfile, type: "child"},

    
        { 
            path: "#", name: "Speciality", icon: "user", type: "dropdown", parentid:"speciality",
                child: [
                    { path: BASEDIR+"/dashboard/Speciality", name: "Speciality"},
                    // { path: BASEDIR+"/dashboard/add-speciality", name: "Add Speciality"},
                    // { path: BASEDIR+"/dashboard/update-speciality", name: "Update Speciality"},
                ]
        },
            { path: BASEDIR+"/dashboard/Speciality", component: Speciality, type: "child"},
            // { path: BASEDIR+"/dashboard/add-speciality", component: AddSpeciality, type: "child"},
            // { path: BASEDIR+"/dashboard/update-speciality", component: UpdateSpeciality, type: "child"},

            { 
                path: "#", name: "Clinics", icon: "user", type: "dropdown", parentid:"clinic",
                    child: [
                        { path: BASEDIR+"/dashboard/clinic", name: "Clinics"},
                        { path: BASEDIR+"/dashboard/add-clinic", name: "Add Clinics"},
                        // { path: BASEDIR+"/dashboard/update-clinic", name: "Update Clinics"},
                      
                    ]
            },
                { path: BASEDIR+"/dashboard/clinic", component: Clinic, type: "child"},
                // { path: BASEDIR+"/dashboard/update-clinic", component: UpdateClinic, type: "child"},
                { path: BASEDIR+"/dashboard/add-clinic", component: AddClinic, type: "child"},
            { 
                path: "#", name: "Procedures", icon: "user", type: "dropdown", parentid:"procedures",
                    child: [
                        { path: BASEDIR+"/dashboard/Procedures", name: "Procedures"},
                       // { path: BASEDIR+"/dashboard/add-procedures", name: "Add Procedures"},
                        // { path: BASEDIR+"/dashboard/update-procedure", name: "UpdateProcedure"},
                      
                    ]
            },
                { path: BASEDIR+"/dashboard/Procedures", component: Procedures, type: "child"},
               // { path: BASEDIR+"/dashboard/add-procedures", component: AddProcedures, type: "child"},
                // {path:BASEDIR+ "/dashboard/update-procedure", component:UpdateProcedure,type:"child"},

    { 
        path: "#", name: "Patients", icon: "people", type: "dropdown", parentid:"partients",
            child: [
                { path: BASEDIR+"/dashboard/patients", name: "Patients"},
                { path: BASEDIR+"/dashboard/add-patient", name: "Add Patient"},
                // { path: BASEDIR+"/dashboard/update-patient", name: "UpdatePatient"},
            //     { path: BASEDIR+"/dashboard/edit-patient", name: "Edit Patient"},
            //    { path: BASEDIR+"/dashboard/patient-profile",name: "Patient Profile"},
               
            ]
    },
        { path: BASEDIR+"/dashboard/patients", component: Patient, type: "child"},
        { path: BASEDIR+"/dashboard/add-patient", component: AddPatient, type: "child"},
        // { path: BASEDIR+"/dashboard/update-patient", component: UpdatePatient, type: "child"},
        // { path: BASEDIR+"/dashboard/edit-patient", component: EditPatient, type: "child"},
        // { path: BASEDIR+"/dashboard/patient-profile/:id", component: PatientProfile, type: "child"},

    { 
        path: "#", name: "Appointments", icon: "calendar", type: "dropdown", parentid:"appointments",
            child: [
                { path: BASEDIR+"/dashboard/appointments", name: "Appointments"},
                { path: BASEDIR+"/dashboard/add-appointment", name: "Add appointment"},
                // { path: BASEDIR+"/dashboard/update-appointment", name: "Update appointment"},
                // { path: BASEDIR+"/dashboard/edit-appointment", name: "Edit Appointment"}, 
            ]
    },
       { path: BASEDIR+"/dashboard/appointments", component: Appointment, type: "child"},
        { path: BASEDIR+"/dashboard/add-appointment", component: AddAppointment, type: "child"},
        // { path: BASEDIR+"/dashboard/update-appointment", component: UpdateAppointment, type: "child"},
        // { path: BASEDIR+"/dashboard/edit-appointment", component: EditAppointment, type: "child"},
 
    // { 
    //     path: "#", name: "Reports", icon: "pie-chart", type: "dropdown", parentid:"reports",
    //     child: [
    //                 { path: BASEDIR+"/dashboard/reports-hospital", name: "Hospital"},
    //                 { path: BASEDIR+"/dashboard/reports-patient", name: "Patients"},
    //                 { path: BASEDIR+"/dashboard/reports-sales", name: "Sales"},
    //                 { path: BASEDIR+"/dashboard/reports-stats", name: "Statistics"},
    //     ]
    // },
    // { path: BASEDIR+"/dashboard/reports-hospital", component: HospitalReportsHospital, type: "child"},
    // { path: BASEDIR+"/dashboard/reports-patient", component: HospitalReportsPatient, type: "child"},
    // { path: BASEDIR+"/dashboard/reports-sales", component: HospitalReportsSales, type: "child"},
    // { path: BASEDIR+"/dashboard/reports-stats", component: HospitalReportsStats, type: "child"},

//  { 
//         path: "#", name: "Billing", icon: "wallet", type: "dropdown", parentid:"billing",
//             child: [
//                 { path: BASEDIR+"/dashboard/payments", name: "Payments"},
//                 { path: BASEDIR+"/dashboard/addpayment", name: "Add Payment"},
//                 { path: BASEDIR+"/dashboard/invoice", name: "Invoice"},
//             ]
//     },
//         { path: BASEDIR+"/dashboard/payments", component: HospitalPayments, type: "child"},
//         { path: BASEDIR+"/dashboard/addpayment", component: HospitalAddPayment, type: "child"},
//         { path: BASEDIR+"/dashboard/invoice", component: HospitalInvoice, type: "child"},

//     { 
//         path: "#", name: "Staffs", icon: "user-female", type: "dropdown", parentid:"staffs",
//             child: [
//                 { path: BASEDIR+"/dashboard/staffs", name: "Staffs"},
//                 { path: BASEDIR+"/dashboard/add-staff", name: "Add Staff"},
//                 { path: BASEDIR+"/dashboard/edit-staff", name: "Edit Staff"},
//                 { path: BASEDIR+"/dashboard/staff-profile", name: "Staff Profile"},
//             ]
//     },
//         { path: BASEDIR+"/dashboard/staffs", component: Staff, type: "child"},
//         { path: BASEDIR+"/dashboard/add-staff", component: AddStaff, type: "child"},
//         { path: BASEDIR+"/dashboard/edit-staff", component: EditStaff, type: "child"},
//         { path: BASEDIR+"/dashboard/staff-profile", component: StaffProfile, type: "child"},

//     { 
//         path: "#", name: "Events", icon: "event", type: "dropdown", parentid:"events",
//             child: [
//                 { path: BASEDIR+"/dashboard/events", name: "Events"},
//                 { path: BASEDIR+"/dashboard/addevent", name: "Add Event"},
//             ]
//     },
//         { path: BASEDIR+"/dashboard/events", component: HospitalEvents, type: "child"},
//         { path: BASEDIR+"/dashboard/addevent", component: AddEvent, type: "child"},

   
//     { path: BASEDIR+"/dashboard/locations", name: "Locations", icon: "directions", component: HospitalLocations },


//     { 
//         path: "#", name: "Mail Box", icon: "envelope", type: "dropdown", parentid:"mailbox",
//         child: [
//             { path: BASEDIR+"/dashboard/mail-inbox", name: "Inbox"},
//             { path: BASEDIR+"/dashboard/mail-compose", name: "Compose"},
//             { path: BASEDIR+"/dashboard/mail-view", name: "View"},
//         ]
//     },
//     { path: BASEDIR+"/dashboard/mail-inbox", component: HospitalMailinbox, type: "child"},
//     { path: BASEDIR+"/dashboard/mail-compose", component: HospitalMailcompose, type: "child"},
//     { path: BASEDIR+"/dashboard/mail-view", component: HospitalMailview, type: "child"},


    { redirect: true, path: BASEDIR+"/", pathTo: BASEDIR+"/dashboard", name: "Dashboard" },
    { redirect: true, path: "/", pathTo: BASEDIR+"/dashboard", name: "Dashboard" }


];
export default dashRoutes;
