import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
var BASEDIR = process.env.REACT_APP_BASEDIR;

class ProtectedRoute extends Component {

    render() {
        const Component = this.props.component;
        const isAuthenticated = window.localStorage.getItem("token");

        return isAuthenticated ? (
            <Component />
        ) : (
                <Redirect to={{ pathname: BASEDIR + "/login" }} />
            );
    }
}

export default ProtectedRoute;